<!DOCTYPE html >

<head>

    <meta charset="utf-8">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"

          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">

    <title></title>

    <style>

        .card {

            font-family: raleway;

            border: 1px solid;

            border-color: #c5c2c28c;

            border-width: 0%;

            border-radius: 50px;

            border-left: 3px solid;

            border-left-color: #ff8484;



        }



        img:hover {

            opacity: 0.8;

        }



        p {

            color: #a8a5a5;

            font-size: 12px;

        }



        h5 {

            border-bottom: 2px solid #f2c94c;

            font-weight: 600;

            color: #949494;



        }



        img {

            box-shadow: 0px 3px 8px -4px rgba(120, 120, 120, 1);

        }



        #boton {

            position: relative;

            float: right;

            margin-bottom: 10px;

            text-decoration: underline;

            color: #007fee;

        }



        #boton:hover {

            color: #79c0ff;

        }

    </style>



</head>

<html>

<body>



<div class="container">

    var contentString =

    <div id="content">

        <div id="siteNotice">

            <div class="card mt-5 shadow mb-5 bg-white rounded-lg " style="width: 15rem;">

                <img src="img/foto.jpeg" class="card-img-top" alt="">

                <div class="card-body">

                    <h5 class="card-title">

                        <div id="firstHeading" class="firstHeading">Uluru</div>

                    </h5>

                    <div id="bodyContent">

                        <p class="card-text " id="bloque">

                        <p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large

                            sandstone rock formation in the southern part of the

                            Northern Territory, central Australia.<br> <!--It lies 335 km (208 mi)

             south west of the nearest large town, Alice Springs; 450 km 

            (280 mi) by road. Kata Tjuta and Uluru are the two major 

              features of the Uluru - Kata Tjuta National Park. Uluru is 

             sacred to the Pitjantjatjara and Yankunytjatjara, the 

            Aboriginal people of the area. It has many springs, waterholes, 

            rock caves and ancient paintings. Uluru is listed as a World 

             Heritage Site</p>

             <p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">

             https://en.wikipedia.org/w/index.php?title=Uluru</a> 

            (last visited June 22, 2009).</p>-->

                            <a href="#" class="btn btn-outline-none btn-sm mt-2" id="boton">Mas Info</a>

                    </div>

                </div>

            </div>





</body>

</html>