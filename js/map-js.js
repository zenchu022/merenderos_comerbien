    var database = firebase.database();
    var map;
    var markers = [];
    var institucionesList = [];
    var comboProvincias = document.getElementById("provinciasList");
    var comboMunicipios = document.getElementById("municipiosList");
    // var comboEnergias = document.getElementById("energiasList");
    var comboServicios = document.getElementById("serviciosList");
    var buttonRefrescar = document.getElementById("buttonRefrescar");
    var atributeTypeEnergia = "TipoDeEnergia";//"energia";

    function initMap() {
        console.log('initMap');
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(-34.603722, -58.381592),
            zoom: 14

        });
    }

    var todosList = [];
    var todasProvincias = [];

    comboMunicipios.setAttribute('disabled', true);
    //comboServicios.setAttribute('disabled', true);
    comboMunicipios.innerHTML = comboMunicipios.innerHTML + "<option selected value='" + 0 + "'>Todas</option>";

    function CargarTodosEnMemoria() {

        database
            .ref('provincias')
            .once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (provinciasSnapshot) {

                    provinciasSnapshot.forEach(function (municipiosSnapshot) {

                        municipiosSnapshot.forEach(function (childSnapshot) {
                            //console.log(childSnapshot.key);
                            childSnapshot.child("merendero").forEach(function (merenderosSnapshot) {
                                merenderosSnapshot.province = provinciasSnapshot.key;
                                merenderosSnapshot.department = childSnapshot.key;
                                merenderosSnapshot.type = 'merendero';
                                todosList.push(merenderosSnapshot);
                            });
                            childSnapshot.child("comederos").forEach(function (comederosSnapshot) {
                                comederosSnapshot.province = provinciasSnapshot.key;
                                comederosSnapshot.department = childSnapshot.key;
                                comederosSnapshot.type = 'comedor';
                                todosList.push(comederosSnapshot);
                            })

                        })

                    })


                });

            })
            .then(function (listado) {
                RellenarDatosTodos();
                agregarTarjetasTodos(0,8,1)

            });
    }

    function RellenarDatosTodos()
    {
        comboProvincias.innerHTML = comboProvincias.innerHTML + "<option selected value='" + 0 + "'>Todas</option>"
        moveCamera(-38.4131324, -63.6808963, 4.8);

        for (let i = 0; i < todosList.length; i++)
        {

            try {
                 //todosList[i].val().DatosDeLaInstitucion.nombreinstitucion.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.provincia.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.distrito.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.calle.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.numero.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.barrio.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.entrecalle.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().InicioDeActividad.año.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().InicioDeActividad.mes.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().InstitucionVinculada.vinculada.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().InstitucionVinculada.descripcion.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().OtrasActividades.otras.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().OtrasActividades.descripcion.ActividadesCulturales + ';' +
                // todosList[i].val().OtrasActividades.descripcion.ActividadesDeportivas + ';' +
                // todosList[i].val().OtrasActividades.descripcion.TalleresOficios + ';' +
                // todosList[i].val().OtrasActividades.descripcion.TerminalidadEducativa + ';' +
                // todosList[i].val().OtrasActividades.descripcion.apoyo + ';' +
                // todosList[i].val().OtrasActividades.descripcion.otras + ';' +
                // todosList[i].val().DiasDeFuncionamiento.domingo.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.domingo.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.lunes.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.lunes.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.martes.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.martes.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.miercoles.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.miercoles.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.jueves.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.jueves.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.viernes.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.viernes.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.sabado.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.sabado.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().ServicioQueOfrece.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().CantidadDeAsistentes.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().CantidadDeComensales.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().TipoDeEnergia.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().Responsable.nombre.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().Responsable.email.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().Responsable.telefono.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().Imagen.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().latitude + ';' +
                // todosList[i].val().longitude + ';' +
                // todosList[i].key.replace(/(\r\n|\n|\r|;)/gm, "") + ';' + '\n';

                todasProvincias.push(todosList[i].val().DatosDeLaInstitucion);

                addMarker(todosList[i].val().latitude,
                    todosList[i].val().longitude,
                    todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                    todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                    todosList[i].val().Imagen,
                    todosList[i].val().DatosDeLaInstitucion.calle,
                    todosList[i].val().DatosDeLaInstitucion.numero,
                    todosList[i].val().DatosDeLaInstitucion.provincia,
                    todosList[i].val().InstitucionVinculada.descripcion)
            }
            catch (e) {
                console.log(e.toString());
            }
        }

        
        let todasProvinciasUnique =  getUniqueProvincias(todasProvincias,'provincia');
        todasProvincias = [];
        todasProvincias = todasProvinciasUnique;
        console.log(todasProvincias);
        for (let i = 0; i < todasProvincias.length; i++)
        {
            let Indice = i + 1;
            comboProvincias.innerHTML = 
            comboProvincias.innerHTML + "<option selected value='" + Indice + "'>" + todasProvincias[i] + "</option>"
        }
        selectElement('provinciasList', 0);
    }

    CargarTodosEnMemoria();

    var todasLocalidades  = [];
    function loadMuniDeMemoria()
    {
        for (let i = 0; i < todosList.length; i++)
        {
            try
            {
                if (comboProvincias[comboProvincias.selectedIndex].innerText === todosList[i].val().DatosDeLaInstitucion.provincia) {
                    todasLocalidades.push(todosList[i].val().DatosDeLaInstitucion);

                }
            }
            catch (e) {
                console.log(e.toString());
            }
        }

        let todasLocalidadesUnique =  getUniqueLocalidades(todasLocalidades,"distrito");
        todasLocalidades = [];
        todasLocalidades = todasLocalidadesUnique;
        for (let i = 0; i < todasLocalidades.length; i++)
        {
            let Indice = i + 1;
            comboMunicipios.innerHTML = comboMunicipios.innerHTML + "<option selected value='" + Indice + "'>" + todasLocalidades[i].distrito + "</option>"
        }
        selectElement('municipiosList', 0);
        deleteAllMarkers();


    }

    comboProvincias.onchange = function () {
        deleteAllMarkers();
        if (comboProvincias[comboProvincias.selectedIndex].innerText === "Todas")
        {
            clearSelect('provinciasList')
            cardsContainer.innerHTML = "";
            moveCamera(-38.4131324, -63.6808963, 4.8);
            selectElement('municipiosList', 0);
            comboMunicipios.setAttribute('disabled', true);
            comboServicios.setAttribute('disabled', true);
            RellenarDatosTodos();
        }
        else
        {
            moveMapConProvinciaDelay();
            loadMuniDeMemoria();
            comboMunicipios.removeAttribute('disabled', true);
            comboServicios.removeAttribute('disabled', true);
        }
    };

    comboMunicipios.onchange = function ()
    {
        if (comboMunicipios[comboMunicipios.selectedIndex].innerText === "Todas") {
            moveMapConProvincia();
        }
        else {
            moveMapConLocalidad();
        }
    };

    function loadLocalidadesTodas()
    {

        for (let i = 0; i < todosList.length; i++)
        {
            try
            {
                if (comboProvincias[comboProvincias.selectedIndex].innerText === todosList[i].val().DatosDeLaInstitucion.provincia) {
                    addMarker(todosList[i].val().latitude,
                        todosList[i].val().longitude,
                        todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                        todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                        todosList[i].val().Imagen,
                        todosList[i].val().DatosDeLaInstitucion.calle,
                        todosList[i].val().DatosDeLaInstitucion.numero,
                        todosList[i].val().DatosDeLaInstitucion.provincia,
                        todosList[i].val().InstitucionVinculada.descripcion)
                }
            }
            catch (e) {
                console.log(e.toString());
            }
        }
    }

    

    function getUniqueProvincias(arr,prov) {
        
        var elementsProvicias = new Array(arr.length);
   
        for (let h = 0 ; h < arr.length; h++) {
            
            if (typeof arr[h] === 'undefined' ) {
               console.log(arr[h]+ " posicion undefined");
            }else{
                elementsProvicias[h]=arr[h].provincia;
            }
 
        }
     
        var listProvisiasConComederosMerenderos = elementsProvicias.filter(function (valor, indiceActual, arreglo) {
            let indiceAlBuscar = arreglo.indexOf(valor);
            if (indiceActual === indiceAlBuscar) {
                return true;
            } else {
                return false;
            }
        });
 

        return listProvisiasConComederosMerenderos;


    }

 



    function getUniqueLocalidades(arr,Localidad) {

        const unique = arr
            .map(e => e[Localidad])

            // store the keys of the unique objects
            .map((e, i, final) => final.indexOf(e) === i && i)

            // eliminate the dead keys & store unique objects
            .filter(e => arr[e]).map(e => arr[e]);

        return unique;
    }


    function getUnique(arr,DatosDeLaInstitucion,NombreInstitucion) {

        const unique = arr
            .map(e => e[DatosDeLaInstitucion][NombreInstitucion])

            // store the keys of the unique objects
            .map((e, i, final) => final.indexOf(e) === i && i)

            // eliminate the dead keys & store unique objects
            .filter(e => arr[e]).map(e => arr[e]);

        return unique;
    }

    function moveMapConProvincia()
    {
        // let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias.options[comboProvincias.selectedIndex].value + ',+' + comboMunicipios.options[comboMunicipios.selectedIndex].value + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias[comboProvincias.selectedIndex].innerText + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
            console.log(url);
            fetch(url)
                .then(function (response) {
                    return response.json();

                })
                .then(function (json) {
                    //console.log(json);
                    moveCamera(json.results[0].geometry.location.lat, json.results[0].geometry.location.lng, 10);
                });
    }

    function moveMapConProvinciaDelay()
    {
        // let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias.options[comboProvincias.selectedIndex].value + ',+' + comboMunicipios.options[comboMunicipios.selectedIndex].value + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias[comboProvincias.selectedIndex].innerText + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
            console.log(url);
            fetch(url)
                .then(function (response) {
                    return response.json();

                })
                .then(function (json) {
                    //console.log(json);
                    moveCamera(json.results[0].geometry.location.lat, json.results[0].geometry.location.lng, 10);
                })
                .then(function (CargarMunicipios) {
                    CargarMarcadoresTodosMunicipios();
                });;
    }

    function moveMapConLocalidad()
    {
        // let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias.options[comboProvincias.selectedIndex].value + ',+' + comboMunicipios.options[comboMunicipios.selectedIndex].value + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias[comboProvincias.selectedIndex].innerText + ',+' + comboMunicipios[comboMunicipios.selectedIndex].innerText + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
        console.log(url);
        fetch(url)
            .then(function (response) {
                return response.json();

            })
            .then(function (json) {
                //console.log(json);
                moveCamera(json.results[0].geometry.location.lat, json.results[0].geometry.location.lng, 13);
            });
    }

    function CargarMarcadoresTodosMunicipios()
    {
        if (comboMunicipios[comboMunicipios.selectedIndex].innerText === "Todas")
        {
            for (let i = 0; i < todosList.length; i++)
            {
                try
                {
                    if (comboProvincias[comboProvincias.selectedIndex].innerText === todosList[i].val().DatosDeLaInstitucion.provincia) {
                        todasLocalidades.push(todosList[i].val().DatosDeLaInstitucion);

                        var valServ = comboServicios.options[comboServicios.selectedIndex].text.toLowerCase().trim().replace(' ', '');

                        if (valServ === "merenderoy comedor" || valServ === "todos") {
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        } else if ((valServ === "merenderos") && (todosList[i].val().ServicioQueOfrece === 'comederos')){
                            addMarker(todosList[i].val().latitude,
                            todosList[i].val().longitude,
                            todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                            todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                            todosList[i].val().Imagen,
                            todosList[i].val().DatosDeLaInstitucion.calle,
                            todosList[i].val().DatosDeLaInstitucion.numero,
                            todosList[i].val().DatosDeLaInstitucion.provincia,
                            todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        } else if ((valServ === "comedores") && (todosList[i].val().ServicioQueOfrece === 'merendero')){
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        }
                    }
                    else if ((comboProvincias[comboProvincias.selectedIndex].innerText === 'Todas') && (comboMunicipios[comboMunicipios.selectedIndex].innerText === 'Todas'))
                    {
                        var valServ = comboServicios.options[comboServicios.selectedIndex].text.toLowerCase().trim().replace(' ', '');

                        if (valServ === "merenderoy comedor" || valServ === "todos") {
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        } else if ((valServ === "merenderos") && (todosList[i].val().ServicioQueOfrece === 'comederos')){
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        } else if ((valServ === "comedores") && (todosList[i].val().ServicioQueOfrece === 'merendero')){
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        }
                    }
                }
                catch (e) {
                    console.log(e.toString());
                }
            }

            agregarTarjetas(0,8);

        }
    }

    buttonRefrescar.onclick = function () {
        console.log("refrescando...");

        var cardsContainer = document.getElementById("cardsContainer");
        cardsContainer.innerHTML = "";

        institucionesList = [];

        deleteAllMarkers();
        if (comboMunicipios[comboMunicipios.selectedIndex].innerText === "Todas")
        {
            CargarMarcadoresTodosMunicipios();

        }
        else
        {
            cargarMarcas(comboProvincias[comboProvincias.selectedIndex].innerText, comboMunicipios[comboMunicipios.selectedIndex].innerText);
            document.getElementById('cardsContainer').innerHTML = "";
            moveMapConLocalidad();
        }

    }

    var agregarTarjetas = function agregarTarjetas(init, last) {
        
        console.log('agregarTarjetas');
        var cardsContainer = document.getElementById("cardsContainer");
        cardsContainer.innerHTML = "";
        let institucionesDistinct =  getUnique(institucionesList,'DatosDeLaInstitucion','nombreinstitucion');

        

        //Paginación
        let num = institucionesDistinct.length;
        let res = num /8;
        let numPaginas = Math.ceil(res);


        for (i = 0; i < institucionesDistinct.length; i++) {
            if (i>= init && i<last)
            {
                var title = institucionesDistinct[i].DatosDeLaInstitucion.nombreinstitucion;
                var description = institucionesDistinct[i].DatosDeLaInstitucion.descripcioninstitucion;
                var image = institucionesDistinct[i].Imagen;
                var calle = institucionesDistinct[i].DatosDeLaInstitucion.calle;
                var nro = institucionesDistinct[i].DatosDeLaInstitucion.numero;
                var provincia = institucionesDistinct[i].DatosDeLaInstitucion.provincia;
                var localidad = institucionesDistinct[i].DatosDeLaInstitucion.distrito;

                image = institucionesDistinct[i].Imagen;
                if (image == "")
                {
                    image = "./img/default.jpg"
                }

                //if (institucionesDistinct[i].add == false) {
                    cardsContainer.innerHTML = cardsContainer.innerHTML +
                        '         <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-around"  id="content">\n' +
                        '            <div class="card mt-5 shadow mb-5 bg-white rounded-lg " style="width: 12rem;" id="siteNotice">\n' +
                        '                <img src=' + image + ' class="card-img-top" alt="">\n' +
                        '            <div class="card-body">\n' +
                        '                <h5 class="card-title"><div id="firstHeading" class="firstHeading">' + institucionesDistinct[i].DatosDeLaInstitucion.nombreinstitucion + ' - ' + localidad + '</div></h5>\n' +
                        '            <div id="bodyContent">\n' +
                        '                <p class="card-text " id="bloque"><p><b>' + institucionesDistinct[i].DatosDeLaInstitucion.nombreinstitucion + '</b>' + institucionesDistinct[i].DatosDeLaInstitucion.descripcioninstitucion.substring(0, 50) + '<br> \n' +
                        "<a href='javascript:openModal(\"" + title + "\",\"" + description + "\",\"" + image + "\",\"" + calle + "\",\"" + nro + "\",\"" + provincia + "\");' class='btn btn-outline-none btn-sm mt-2' id='boton'>Mas Info</a>" + '\n' +
                        '            </div>\n' +
                        '            </div>\n' +
                        '            </div>\n' +
                        '        </div> \n'
                    ;

                    //institucionesDistinct[i].add = true;
                //}
            }
        }
        if (numPaginas > 1)
        {
            var prev = 0;
            var prevLast = 0;
            if (init != 0) prev = init - 8;
            var next = 8;
            var nextLast = 8;
            if (next != 8) next = last + 8;
            prevLast = prev + 8;
            nextLast = next + 8;

            var inicio = 0;
            var fin = 8;

            cardsContainer.innerHTML = cardsContainer.innerHTML + '<div class="col-12 "/>\n';
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '   <nav>\n' +
                '       <ul class="pagination">\n' +
                '           <li class="page-item"><a  href="javascript:agregarTarjetas(' + prev + ',' + prevLast +');" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span></a></li>\n';
            for (i=1; i <= numPaginas; i++)
            {
                cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetas(' + inicio + ',' + fin +');" class="page-link">' + i + '</a></li>\n';
                inicio= inicio + 8;
                fin = fin + 8;
            }
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetas(' + next + ',' + nextLast +');" class="page-link" aria-label="Next"><span aria-hidden="true">»</span></a></li>\n' +
                '       </ul>\n' +
                '    </nav>\n';
        }
    };


    //Agregar Card y Paginacion de las mismas 
    var agregarTarjetasTodos = function agregarTarjetasTodos(init, last,idli) {
        console.log('agregarTarjetas');
        var cardsContainer = document.getElementById("cardsContainer");
        cardsContainer.innerHTML = "";
        //let institucionesDistinct =  getUnique(todosList,'DatosDeLaInstitucion','nombreinstitucion');

        //console.log(todosList);

        //Paginación

        let num = todosList.length;
        let res = num /8;
        let numPaginas = Math.ceil(res);

        console.log(idli);
        console.log("=============");
        if(idli==0) idli=1; 

        
        console.log(idli*8);
        console.log(idli);

        
        for (i = 0; i < todosList.length; i++) {

            


            if (i>= ((idli*8)-8) && i<idli*8)
            {
                var title = todosList[i].val().DatosDeLaInstitucion.nombreinstitucion;
                var description = todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion;
                var image = todosList[i].val().Imagen;
                var calle = todosList[i].val().DatosDeLaInstitucion.calle;
                var nro = todosList[i].val().DatosDeLaInstitucion.numero;
                var provincia = todosList[i].val().DatosDeLaInstitucion.provincia;
                var localidad = todosList[i].val().DatosDeLaInstitucion.distrito;

                image = todosList[i].val().Imagen;
                if (image == "")
                {
                    image = "./img/default.jpg"
                }

                //if (institucionesDistinct[i].add == false) {
                cardsContainer.innerHTML = cardsContainer.innerHTML +
                    '         <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-around"  id="content">\n' +
                    '            <div class="card mt-5 shadow mb-5 bg-white rounded-lg " style="width: 12rem;" id="siteNotice">\n' +
                    '                <img src=' + image + ' class="card-img-top" alt="">\n' +
                    '            <div class="card-body">\n' +
                    '            <small>' + localidad + '</small>\n' +
                    '                <h5 class="card-title"><div id="firstHeading" class="firstHeading">' + todosList[i].val().DatosDeLaInstitucion.nombreinstitucion + '</div></h5>\n' +
                    '            <div id="bodyContent">\n' +
                    '                <p class="card-text " id="bloque"><p>' + todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion.substring(0, 50) + '<br> \n' +
                    "<a href='javascript:openModal(\"" + title + "\",\"" + description + "\",\"" + image + "\",\"" + calle + "\",\"" + nro + "\",\"" + provincia + "\");' class='btn btn-outline-none btn-sm mt-2' id='boton'>Mas Info</a>" + '\n' +
                    '            </div>\n' +
                    '            </div>\n' +
                    '            </div>\n' +
                    '        </div> \n'
                ;

                
            }



        }


        if (numPaginas > 1)
        {

            console.log(numPaginas + " num paginas");

            var prev = 0;
            var prevLast = 0;
            if (init != 0) prev = init - 8;
            prevLast = prev + 8;

            console.log("prev: "+prev);
            console.log("prev Last: "+prevLast);

            var next = 8;
            var nextLast = 8;
            if (last != 8) next = last + 8;
            nextLast = next + 8;

            console.log("next: "+next);
            console.log("next Last: "+nextLast);



            //---------------
            


            cardsContainer.innerHTML = cardsContainer.innerHTML + '<div class="col-12 "/>\n';
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '   <nav>\n' +
                '       <ul class="pagination">\n' +
                '           <li class="page-item"><a href="javascript:agregarTarjetasTodos(' + 0 + ',' + 8 +','+1+');" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span></a></li>\n';

            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetasTodos(' + prev + ',' + prevLast +','+prev+');" class="page-link" aria-label="Previous"><span aria-hidden="true">Anterior</span></a></li>\n';

            console.log("init: "+init);
            console.log("last: "+last);
            console.log(numPaginas);
            for (i=(init+1); i <= last; i++)
            {
                console.log(i);
                console.log(todosList.length-7);
                if (i<= (numPaginas) ) {
                    cardsContainer.innerHTML = cardsContainer.innerHTML +
                    '       <li class="page-item"><a id="'+i+'li" href="javascript:agregarTarjetasTodos(' + init + ',' + last +','+i+');" class="page-link">' + i + '</a></li>\n';
                }
                
            }

            if (next>=40) {
                next= next-8;
                nextLast= nextLast-8;
            }
            console.log("next: "+next);
            console.log("nextLast: "+nextLast);
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetasTodos(' + next + ',' + nextLast +','+next+');" class="page-link" aria-label="Next"><span aria-hidden="true">Siguiente</span></a></li>\n' ;

            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetasTodos(' + (numPaginas-8) + ',' + numPaginas +','+(numPaginas-8)+');" class="page-link" aria-label="Next"><span aria-hidden="true">»</span></a></li>\n' +
                '       </ul>\n' +
                '    </nav>\n';
        }
    };


    function addMarker(lat, lng, title, description, image, calle, nro, provincia, institucionVinculada) {
        /*
        console.log("addMarker: ");
        console.log("lat: " + lat);
        console.log("lng: " + lng);
        console.log("title: " + title);
        console.log("description: " + description);
        console.log("Institución Vinculada: " + institucionVinculada);
        */
        let Organizacion = '';

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            title: title,
            icon: 'img/IconComerBien03.png',
        });

        if (image == "")
        {
            image = "./img/default.jpg"
        }

        if (institucionVinculada == null || institucionVinculada == 'undefined')
        {
            Organizacion = "";
        }
        else
        {
            Organizacion = 'Organización: ' + institucionVinculada;
        }

        var contentString =
            '        <div id="siteNotice ">\n' +
            '            <div class="card row no-gutters shadow bg-white rounded-lg overflow-hidden" style=" max-height: 240px;">\n' +
            '               <div class="row no-gutters">\n' +
            '                   <div class="col-md-4">\n' +
            '                <img src="' + image + '" class="card-img" alt="">\n' +
            '                   </div>\n' +
            '               <div class="col-md-8 ">\n' +
            '                <div class="card-body">\n' +
            '                    <h5 class="card-title">\n' +
            '                        <div id="firstHeading" class="firstHeading">' + title + '</div>\n' +
            '                    </h5>\n' +
            '                    <div id="bodyContent">\n' +
            '                        <p class="card-text" id="bloque" >\n' +
            '                        <p class="overflow-hidden"><b></b> Provincia de ' + provincia + ', con dirección:' + calle + ' ' + nro + '\n' + Organizacion + '\n' +
            '                    </div>\n' +
            '                </div>\n' +
            "<a href='javascript:openModal(\"" + title + "\",\"" + description + "\",\"" + image + "\",\"" + calle + "\",\"" + nro + "\",\"" + provincia + "\");' class='btn btn-outline-none btn-sm mt-2' id='boton'>Mas Info</a>" + '\n' +
            '              </div>\n' +
            '               </div>\n' +
            '             </div>\n' +
            '          </div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString,

        });


        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        markers.push(marker);
    }

    function deleteAllMarkers() {
        //Loop through all the markers and remove
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    };

    function moveCamera(lat, lng, zoom) {
        if (zoom == null) zoom = 13;
        console.log("move camera ");
        console.log("lat: " + lat);
        console.log("lng: " + lng);
        if (lat == null && lng == null) {
            console.log("move camera returning...");
            return;
        }
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: {lat: lat, lng: lng},
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            fullscreenControl: true
        });

    }

    function selectElement(id, valueToSelect) {
        var element = document.getElementById(id);
        element.selectedIndex = valueToSelect;
        // element.value = valueToSelect;
    }




    ///=================================================================

    function loadTodos(provinciaId, partidoId, servicios){
        console.log("loadTodos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId;
        console.log("ref: " + reference);

        if (servicios === "merenderoy comedor") {

            database
                .ref(reference)
                .once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function (childSnapshot) {
                        //console.log(childSnapshot.key);
                        //console.log(childSnapshot.val());
                        childSnapshot.forEach(function (childSnapshots) {
                            addMarker(
                                childSnapshots.val().latitude,
                                childSnapshots.val().longitude,
                                childSnapshots.val().DatosDeLaInstitucion.nombreinstitucion,
                                childSnapshots.val().DatosDeLaInstitucion.descripcioninstitucion,
                                childSnapshots.val().Imagen,
                                childSnapshots.val().DatosDeLaInstitucion.calle,
                                childSnapshots.val().DatosDeLaInstitucion.numero,
                                childSnapshots.val().DatosDeLaInstitucion.provincia
                            );

                            var obj = childSnapshots.val();
                            obj.add = false;
                            institucionesList.push(obj);
                        })
                    });
                })
                .then(function (tarjetas) {
                    agregarTarjetas(0, 8);
                });
        }
        else if (servicios === 'todos')
        {
            database
                .ref(reference)
                .once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function (childSnapshot) {
                        //console.log(childSnapshot.key);
                        //console.log(childSnapshot.val());
                        childSnapshot.forEach(function (childSnapshots) {
                            addMarker(
                                childSnapshots.val().latitude,
                                childSnapshots.val().longitude,
                                childSnapshots.val().DatosDeLaInstitucion.nombreinstitucion,
                                childSnapshots.val().DatosDeLaInstitucion.descripcioninstitucion,
                                childSnapshots.val().Imagen,
                                childSnapshots.val().DatosDeLaInstitucion.calle,
                                childSnapshots.val().DatosDeLaInstitucion.numero,
                                childSnapshots.val().DatosDeLaInstitucion.provincia
                            );

                            var obj = childSnapshots.val();
                            obj.add = false;
                            institucionesList.push(obj);
                        })
                    });
                })
                .then(function (tarjetas) {
                    agregarTarjetasTodos(0, 8,1);
                });
        }
    }


    function loadMerenderos(provinciaId, partidoId) {
        console.log("loadMerenderos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId + "/merendero";
        console.log("ref: " + reference);

        database
            .ref(reference)
            .once('value')
            .then(function (snapshot) {
                //console.log(snapshot.key);
                snapshot.forEach(function (childSnapshot) {
                    //console.log(childSnapshot.key);
                    //console.log(childSnapshot.val());


                    addMarker(
                        childSnapshot.val().latitude,
                        childSnapshot.val().longitude,
                        childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion,
                        childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion,
                        childSnapshot.val().Imagen,
                        childSnapshot.val().DatosDeLaInstitucion.calle,
                        childSnapshot.val().DatosDeLaInstitucion.numero,
                        childSnapshot.val().DatosDeLaInstitucion.provincia
                    );

                    var obj = childSnapshot.val();
                    obj.add = false;
                    institucionesList.push(obj);
                });
            })
            .then(function (tarjetas) {
                agregarTarjetas(0, 8);
            });

    }

    function loadComederos(provinciaId, partidoId) {
        console.log("loadComederos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId + "/comederos";
        console.log("ref: " + reference);


        database
            .ref(reference)
            .once('value')
            .then(function (snapshot) {
                //console.log(snapshot.key);
                snapshot.forEach(function (childSnapshot) {
                    //console.log(childSnapshot.key);
                    console.log(childSnapshot.val());

                    addMarker(
                        childSnapshot.val().latitude,
                        childSnapshot.val().longitude,
                        childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion,
                        childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion,
                        childSnapshot.val().Imagen,
                        childSnapshot.val().DatosDeLaInstitucion.calle,
                        childSnapshot.val().DatosDeLaInstitucion.numero,
                        childSnapshot.val().DatosDeLaInstitucion.provincia,
                        childSnapshot.val().InstitucionVinculada.descripcion
                    );

                    var obj = childSnapshot.val();
                    obj.add = false;
                    institucionesList.push(obj);

                });
            })
            .then(function (tarjetas) {
                agregarTarjetas(0, 8);
            });

    }

    var clearSelect = function removeOptions(documentId) {
        selectbox = document.getElementById(documentId);
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    switch (document.getElementById("serviciosList").value)
    {
        case "Merendero y Comedor":
            loadTodos();
            break;
        case "Todos":
            loadTodos();
            break;
        case "Merenderos":
            loadMerenderos();
            break;
        case "Comedores":
            loadComederos();
            break;
    }

    function cargarMarcas(provinciaId, municipioId) {

        database
            .ref("provincias/" + provinciaId + "/partidos/" + municipioId)
            .once('value')
            .then(function (snapshot) {
                moveCamera(snapshot.val().latitude, snapshot.val().longitude, snapshot.val().zoom);

                var valServ = comboServicios.options[comboServicios.selectedIndex].text.toLowerCase().trim().replace(' ', '');

                console.log("valServ: " + valServ);
                // console.log("valEnerg: " + valEnerg);

                if (valServ === "merenderoy comedor") {
                    console.log("camino 1")
                    loadTodos(provinciaId, municipioId, valServ);

                } else if (valServ === "merenderos") {
                    console.log("camino 2");
                    loadMerenderos(provinciaId, municipioId);

                    console.log("camino 3");
                } else if (valServ === "comedores") {
                    loadComederos(provinciaId, municipioId);

                }
                else if (valServ === "todos") {
                    loadTodos(provinciaId, municipioId, valServ);

                }
            });

    }


    //===================================================================================

     // Get the modal
     var modal = document.getElementById('myModal');
     var span = document.getElementsByClassName("close")[0];
 
     span.onclick = function () {
         modal.style.display = "none";
     }
 
     window.onclick = function (event) {
         if (event.target == modal) {
             modal.style.display = "none";
         }
     }
 
     var openModal = function (title, description, image, calle, nro, provincia) {
         console.log('openModal ');
 
         var content = document.getElementById('content-dialog');
 
         content.innerHTML =
             '        <div id="siteNotice ">\n' +
             '            <div class="card row no-gutters shadow bg-white rounded-lg overflow-hidden" style=" max-height: 240px;">\n' +
             '               <div class="row no-gutters">\n' +
             '                   <div class="col-md-4">\n' +
             '                <img src="' + image + '" class="card-img" alt="">\n' +
             '                   </div>\n' +
             '               <div class="col-md-8 ">\n' +
             '                <div class="card-body" style="height: 250px;overflow-y:scroll;">\n' +
             '                    <h5 class="card-title">\n' +
             '                        <div id="firstHeading" class="firstHeading">' + title + '</div>\n' +
             '                    </h5>\n' +
             '                    <div id="bodyContent" >\n' +
             '                        <p class="card-text" id="bloque" >\n' +
             '                        <p class="overflow-hidden"><b></b> ' + description + '\n' +
             '                <p class="card-text " id="bloque"><p><b>' + 'Provincia de:' + provincia + ',' + calle + ' ' + nro + '</b><br> \n' +
             '                    </div>\n' +
             '                </div>\n' +
             '              </div>\n' +
             '               </div>\n' +
             '             </div>\n' +
             '          </div>';
 
 
         modal.style.display = "block";
 
 
     }