<!DOCTYPE html >
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <!-- <link rel="stylesheet" href="estilos.css"> -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=ABeeZee" rel="stylesheet">


    <title>Mapa de Comederos y Merenderos</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 450px;
        }


        .DivToScroll{
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3B3C3E;
            font-size: 12px;
            font-weight: bold;
            left: -1px;
            padding: 10px 7px 5px;
        }

        .DivWithScroll{
            height:120px;
            overflow:scroll;
            overflow-x:hidden;
        }

        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
            font-family: 'ABeeZee', sans-serif;
        }

        h5 {
            font-family: 'ABeeZee', sans-serif;
            font-size: 15px;
            border-bottom: 2px solid #f2c94c;
            font-weight: 600;
            color: #949494;

        }

        #bloque {
            margin-top: -15px;
            font-size: 11px;
            position: absolute;
            float: left;
        }

        #boton {
            text-decoration: underline;
            color: #007fee;
            position: relative;
            float: right;
        }

        #boton:hover {
            color: #79c0ff;

        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
            display: block;
        }
        
        .page-item {list-style: none}
        .card-body h5{font-size:1em }
        .card-body p{font-size:0.8em }
        .card-body small{font-size:0.7em; color: #17a2b8 }

       h5 {
    		font-family: 'ABeeZee', sans-serif;
    		font-size: 1.1em;
    		border-bottom: 2px solid #17a2b8;
    		font-weight: 600;
    		color: #232C31;
    		}

    	#boton {
    text-decoration: underline;
    color: #17a2b8;
    position: relative;
    float: right;
			}
			a {
    color: #17a2b8;
    text-decoration: none;
    background-color: transparent;
}
        /* 
        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -ms-overflow-style: scrollbar;
            -webkit-tap-highlight-color: transparent;
        }

        body {
            margin: 0;
            font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }*/
        

    </style>
</head>

<body>
<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <div id="content-dialog"></div>
    </div>

</div>

<!-- Filtros -->
<div class="container">
    <form>
        <div id="filterCitys" class="row shadow p-3 mb-2 bg-light rounded mt-2 formulario">
            <div class="col-12"><h3>Parámetros de búsqueda</h3></div>    
            <div class="col-12 col-sm-3 p-2">
                <label style="color:#17a2b8"><small>Provincia</small></label>
                <select class="custom-select" id="provinciasList">
                </select>
            </div>
            <div class="col-12 col-sm-3  p-2 ">
                <label style="color:#17a2b8"><small>Localidad</small></label>
                <select class="custom-select" id="municipiosList">
                </select>
            </div>
            <div class="col-12 col-sm-3  p-2 ">
                <label style="color:#17a2b8"><small>Servicios</small></label>
                <select class="custom-select" id="serviciosList">
                    <option selected value=0>Merenderos</option>
                    <option selected value=1>Comedores</option>
                    <option selected value=2>Merendero y Comedor</option>
                    <option selected value=3>Todos</option>
                </select>
            </div>         
            <div class="col-12 col-sm-3  p-2 ">
                <label>&nbsp;</label><br>       
                <a id='buttonRefrescar' class="btn btn-info" style="color:#fff">Localizar</a>
            </div>       
        </div>
    </form>
</div>
<!-- /Filtros -->

<!-- Mapa -->
<div class="container-fluid" style="padding: 0 !important">
    <div class="row">
        <div id="map" class="col map-container img-fluid shadow p-3 mb-5"></div>
    </div>
</div>
<!-- /Mapa -->

<!-- Cards -->
<div class="container">
    <div id="cardsContainer" class="row" style="height: 500px;overflow-y:scroll;"/></div>
</div>
<!-- /Cards -->

<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase.js"></script>
<script>
    // Initialize Firebase
    console.log('Initialize Firebase');
    var config = {
        apiKey: "AIzaSyBwfELRbz6TWmqEQAjKWI4u7aQ9V8FL8Nw",
        authDomain: "comer-bien.firebaseapp.com",
        databaseURL: "https://comer-bien.firebaseio.com",
        projectId: "comer-bien",
        storageBucket: "comer-bien.appspot.com",
        messagingSenderId: "471498877486"
    }
    firebase.initializeApp(config);
    //Carga todos los datos en memoria
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt4-eHHq80AgKWr9fOgM9m7eZbyqZpYDI&callback=initMap">
</script>
<script>
    var database = firebase.database();
    var map;
    var markers = [];
    var institucionesList = [];
    var comboProvincias = document.getElementById("provinciasList");
    var comboMunicipios = document.getElementById("municipiosList");
    // var comboEnergias = document.getElementById("energiasList");
    var comboServicios = document.getElementById("serviciosList");
    var buttonRefrescar = document.getElementById("buttonRefrescar");
    var atributeTypeEnergia = "TipoDeEnergia";//"energia";

    function initMap() {
        console.log('initMap');
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(-34.603722, -58.381592),
            zoom: 14

        });
    }

    var todosList = [];
    var todasProvincias = [];

    comboMunicipios.setAttribute('disabled', true);
    //comboServicios.setAttribute('disabled', true);
    comboMunicipios.innerHTML = comboMunicipios.innerHTML + "<option selected value='" + 0 + "'>Todas</option>";

    function CargarTodosEnMemoria() {

        database
            .ref('provincias')
            .once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (provinciasSnapshot) {

                    provinciasSnapshot.forEach(function (municipiosSnapshot) {

                        municipiosSnapshot.forEach(function (childSnapshot) {
                            //console.log(childSnapshot.key);
                            childSnapshot.child("merendero").forEach(function (merenderosSnapshot) {
                                merenderosSnapshot.province = provinciasSnapshot.key;
                                merenderosSnapshot.department = childSnapshot.key;
                                merenderosSnapshot.type = 'merendero';
                                todosList.push(merenderosSnapshot);
                            });
                            childSnapshot.child("comederos").forEach(function (comederosSnapshot) {
                                comederosSnapshot.province = provinciasSnapshot.key;
                                comederosSnapshot.department = childSnapshot.key;
                                comederosSnapshot.type = 'comedor';
                                todosList.push(comederosSnapshot);
                            })

                        })

                    })


                });

            })
            .then(function (listado) {
                RellenarDatosTodos();
                agregarTarjetasTodos(0,8)

            });
    }

    function RellenarDatosTodos()
    {
        comboProvincias.innerHTML = comboProvincias.innerHTML + "<option selected value='" + 0 + "'>Todas</option>"
        moveCamera(-38.4131324, -63.6808963, 4.8);

        for (let i = 0; i < todosList.length; i++)
        {
            try {
                // todosList[i].val().DatosDeLaInstitucion.nombreinstitucion.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.provincia.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.distrito.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.calle.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.numero.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.barrio.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DatosDeLaInstitucion.entrecalle.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().InicioDeActividad.año.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().InicioDeActividad.mes.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().InstitucionVinculada.vinculada.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().InstitucionVinculada.descripcion.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().OtrasActividades.otras.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().OtrasActividades.descripcion.ActividadesCulturales + ';' +
                // todosList[i].val().OtrasActividades.descripcion.ActividadesDeportivas + ';' +
                // todosList[i].val().OtrasActividades.descripcion.TalleresOficios + ';' +
                // todosList[i].val().OtrasActividades.descripcion.TerminalidadEducativa + ';' +
                // todosList[i].val().OtrasActividades.descripcion.apoyo + ';' +
                // todosList[i].val().OtrasActividades.descripcion.otras + ';' +
                // todosList[i].val().DiasDeFuncionamiento.domingo.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.domingo.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.lunes.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.lunes.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.martes.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.martes.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.miercoles.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.miercoles.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.jueves.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.jueves.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.viernes.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.viernes.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().DiasDeFuncionamiento.sabado.dia + ';' +
                // todosList[i].val().DiasDeFuncionamiento.sabado.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().ServicioQueOfrece.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().CantidadDeAsistentes.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().CantidadDeComensales.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().TipoDeEnergia.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().Responsable.nombre.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().Responsable.email.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().Responsable.telefono.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().Imagen.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                // todosList[i].val().latitude + ';' +
                // todosList[i].val().longitude + ';' +
                // todosList[i].key.replace(/(\r\n|\n|\r|;)/gm, "") + ';' + '\n';

                todasProvincias.push(todosList[i].val().DatosDeLaInstitucion);

                addMarker(todosList[i].val().latitude,
                    todosList[i].val().longitude,
                    todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                    todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                    todosList[i].val().Imagen,
                    todosList[i].val().DatosDeLaInstitucion.calle,
                    todosList[i].val().DatosDeLaInstitucion.numero,
                    todosList[i].val().DatosDeLaInstitucion.provincia,
                    todosList[i].val().InstitucionVinculada.descripcion)
            }
            catch (e) {
                console.log(e.toString());
            }
        }
        let todasProvinciasUnique =  getUniqueProvincias(todasProvincias,'provincia');
        todasProvincias = [];
        todasProvincias = todasProvinciasUnique;
        for (let i = 0; i < todasProvincias.length; i++)
        {
            let Indice = i + 1;
            comboProvincias.innerHTML = comboProvincias.innerHTML + "<option selected value='" + Indice + "'>" + todasProvincias[i].provincia + "</option>"
        }
        selectElement('provinciasList', 0);
    }

    CargarTodosEnMemoria();

    var todasLocalidades  = [];
    function loadMuniDeMemoria()
    {
        for (let i = 0; i < todosList.length; i++)
        {
            try
            {
                if (comboProvincias[comboProvincias.selectedIndex].innerText === todosList[i].val().DatosDeLaInstitucion.provincia) {
                    todasLocalidades.push(todosList[i].val().DatosDeLaInstitucion);

                }
            }
            catch (e) {
                console.log(e.toString());
            }
        }

        let todasLocalidadesUnique =  getUniqueLocalidades(todasLocalidades,"distrito");
        todasLocalidades = [];
        todasLocalidades = todasLocalidadesUnique;
        for (let i = 0; i < todasLocalidades.length; i++)
        {
            let Indice = i + 1;
            comboMunicipios.innerHTML = comboMunicipios.innerHTML + "<option selected value='" + Indice + "'>" + todasLocalidades[i].distrito + "</option>"
        }
        selectElement('municipiosList', 0);
        deleteAllMarkers();


    }

    comboProvincias.onchange = function () {
        deleteAllMarkers();
        if (comboProvincias[comboProvincias.selectedIndex].innerText === "Todas")
        {
            clearSelect('provinciasList')
            cardsContainer.innerHTML = "";
            moveCamera(-38.4131324, -63.6808963, 4.8);
            selectElement('municipiosList', 0);
            comboMunicipios.setAttribute('disabled', true);
            comboServicios.setAttribute('disabled', true);
            RellenarDatosTodos();
        }
        else
        {
            moveMapConProvincia();
            loadMuniDeMemoria();
            comboMunicipios.removeAttribute('disabled', true);
            comboServicios.removeAttribute('disabled', true);
        }
    };

    comboMunicipios.onchange = function ()
    {
        if (comboMunicipios[comboMunicipios.selectedIndex].innerText === "Todas") {
            moveMapConProvincia();
        }
        else {
            moveMapConLocalidad();
        }
    };

    function loadLocalidadesTodas()
    {

        for (let i = 0; i < todosList.length; i++)
        {
            try
            {
                if (comboProvincias[comboProvincias.selectedIndex].innerText === todosList[i].val().DatosDeLaInstitucion.provincia) {
                    addMarker(todosList[i].val().latitude,
                        todosList[i].val().longitude,
                        todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                        todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                        todosList[i].val().Imagen,
                        todosList[i].val().DatosDeLaInstitucion.calle,
                        todosList[i].val().DatosDeLaInstitucion.numero,
                        todosList[i].val().DatosDeLaInstitucion.provincia,
                        todosList[i].val().InstitucionVinculada.descripcion)
                }
            }
            catch (e) {
                console.log(e.toString());
            }
        }
    }

    function getUniqueProvincias(arr,Provincias) {

        const unique = arr
            .map(e => e[Provincias])

            // store the keys of the unique objects
            .map((e, i, final) => final.indexOf(e) === i && i)

            // eliminate the dead keys & store unique objects
            .filter(e => arr[e]).map(e => arr[e]);

        return unique;
    }

    function getUniqueLocalidades(arr,Localidad) {

        const unique = arr
            .map(e => e[Localidad])

            // store the keys of the unique objects
            .map((e, i, final) => final.indexOf(e) === i && i)

            // eliminate the dead keys & store unique objects
            .filter(e => arr[e]).map(e => arr[e]);

        return unique;
    }


    function getUnique(arr,DatosDeLaInstitucion,NombreInstitucion) {

        const unique = arr
            .map(e => e[DatosDeLaInstitucion][NombreInstitucion])

            // store the keys of the unique objects
            .map((e, i, final) => final.indexOf(e) === i && i)

            // eliminate the dead keys & store unique objects
            .filter(e => arr[e]).map(e => arr[e]);

        return unique;
    }

    function moveMapConProvincia()
    {
        // let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias.options[comboProvincias.selectedIndex].value + ',+' + comboMunicipios.options[comboMunicipios.selectedIndex].value + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias[comboProvincias.selectedIndex].innerText + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
            console.log(url);
            fetch(url)
                .then(function (response) {
                    return response.json();

                })
                .then(function (json) {
                    //console.log(json);
                    moveCamera(json.results[0].geometry.location.lat, json.results[0].geometry.location.lng, 10);
                });
    }

    function moveMapConLocalidad()
    {
        // let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias.options[comboProvincias.selectedIndex].value + ',+' + comboMunicipios.options[comboMunicipios.selectedIndex].value + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias[comboProvincias.selectedIndex].innerText + ',+' + comboMunicipios[comboMunicipios.selectedIndex].innerText + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
        console.log(url);
        fetch(url)
            .then(function (response) {
                return response.json();

            })
            .then(function (json) {
                //console.log(json);
                moveCamera(json.results[0].geometry.location.lat, json.results[0].geometry.location.lng, 13);
            });
    }

    buttonRefrescar.onclick = function () {
        console.log("refrescando...");

        var cardsContainer = document.getElementById("cardsContainer");
        cardsContainer.innerHTML = "";

        institucionesList = [];

        deleteAllMarkers();
        if (comboMunicipios[comboMunicipios.selectedIndex].innerText === "Todas")
        {
            for (let i = 0; i < todosList.length; i++)
            {
                try
                {
                    if (comboProvincias[comboProvincias.selectedIndex].innerText === todosList[i].val().DatosDeLaInstitucion.provincia) {
                        todasLocalidades.push(todosList[i].val().DatosDeLaInstitucion);

                        var valServ = comboServicios.options[comboServicios.selectedIndex].text.toLowerCase().trim().replace(' ', '');

                        if (valServ === "merenderoy comedor" || valServ === "todos") {
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        } else if ((valServ === "merenderos") && (todosList[i].val().ServicioQueOfrece === 'comederos')){
                            addMarker(todosList[i].val().latitude,
                            todosList[i].val().longitude,
                            todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                            todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                            todosList[i].val().Imagen,
                            todosList[i].val().DatosDeLaInstitucion.calle,
                            todosList[i].val().DatosDeLaInstitucion.numero,
                            todosList[i].val().DatosDeLaInstitucion.provincia,
                            todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        } else if ((valServ === "comedores") && (todosList[i].val().ServicioQueOfrece === 'merendero')){
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        }
                    }
                    else if ((comboProvincias[comboProvincias.selectedIndex].innerText === 'Todas') && (comboMunicipios[comboMunicipios.selectedIndex].innerText === 'Todas'))
                    {
                        var valServ = comboServicios.options[comboServicios.selectedIndex].text.toLowerCase().trim().replace(' ', '');

                        if (valServ === "merenderoy comedor" || valServ === "todos") {
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        } else if ((valServ === "merenderos") && (todosList[i].val().ServicioQueOfrece === 'comederos')){
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        } else if ((valServ === "comedores") && (todosList[i].val().ServicioQueOfrece === 'merendero')){
                            addMarker(todosList[i].val().latitude,
                                todosList[i].val().longitude,
                                todosList[i].val().DatosDeLaInstitucion.nombreinstitucion,
                                todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion,
                                todosList[i].val().Imagen,
                                todosList[i].val().DatosDeLaInstitucion.calle,
                                todosList[i].val().DatosDeLaInstitucion.numero,
                                todosList[i].val().DatosDeLaInstitucion.provincia,
                                todosList[i].val().InstitucionVinculada.descripcion)

                            var obj = todosList[i].val();
                            obj.add = false;
                            institucionesList.push(obj);

                        }
                    }
                }
                catch (e) {
                    console.log(e.toString());
                }
            }

            agregarTarjetas(0,8);

        }
        else
        {
            cargarMarcas(comboProvincias[comboProvincias.selectedIndex].innerText, comboMunicipios[comboMunicipios.selectedIndex].innerText);
            document.getElementById('cardsContainer').innerHTML = "";
            moveMapConLocalidad();
        }

    }

    var agregarTarjetas = function agregarTarjetas(init, last) {
        console.log('agregarTarjetas');
        var cardsContainer = document.getElementById("cardsContainer");
        cardsContainer.innerHTML = "";
        let institucionesDistinct =  getUnique(institucionesList,'DatosDeLaInstitucion','nombreinstitucion');

        //Paginación
        let num = institucionesDistinct.length;
        let res = num /8;
        let numPaginas = Math.ceil(res);


        for (i = 0; i < institucionesDistinct.length; i++) {
            if (i>= init && i<last)
            {
                var title = institucionesDistinct[i].DatosDeLaInstitucion.nombreinstitucion;
                var description = institucionesDistinct[i].DatosDeLaInstitucion.descripcioninstitucion;
                var image = institucionesDistinct[i].Imagen;
                var calle = institucionesDistinct[i].DatosDeLaInstitucion.calle;
                var nro = institucionesDistinct[i].DatosDeLaInstitucion.numero;
                var provincia = institucionesDistinct[i].DatosDeLaInstitucion.provincia;
                var localidad = institucionesDistinct[i].DatosDeLaInstitucion.distrito;

                image = institucionesDistinct[i].Imagen;
                if (image == "")
                {
                    image = "./img/default.jpg"
                }

                //if (institucionesDistinct[i].add == false) {
                    cardsContainer.innerHTML = cardsContainer.innerHTML +
                        '         <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-around"  id="content">\n' +
                        '            <div class="card mt-5 shadow mb-5 bg-white rounded-lg " style="width: 12rem;" id="siteNotice">\n' +
                        '                <img src=' + image + ' class="card-img-top" alt="">\n' +
                        '            <div class="card-body">\n' +
                        '                <h5 class="card-title"><div id="firstHeading" class="firstHeading">' + institucionesDistinct[i].DatosDeLaInstitucion.nombreinstitucion + ' - ' + localidad + '</div></h5>\n' +
                        '            <div id="bodyContent">\n' +
                        '                <p class="card-text " id="bloque"><p><b>' + institucionesDistinct[i].DatosDeLaInstitucion.nombreinstitucion + '</b>' + institucionesDistinct[i].DatosDeLaInstitucion.descripcioninstitucion.substring(0, 50) + '<br> \n' +
                        "<a href='javascript:openModal(\"" + title + "\",\"" + description + "\",\"" + image + "\",\"" + calle + "\",\"" + nro + "\",\"" + provincia + "\");' class='btn btn-outline-none btn-sm mt-2' id='boton'>Mas Info</a>" + '\n' +
                        '            </div>\n' +
                        '            </div>\n' +
                        '            </div>\n' +
                        '        </div> \n'
                    ;

                    //institucionesDistinct[i].add = true;
                //}
            }
        }
        if (numPaginas > 1)
        {
            var prev = 0;
            var prevLast = 0;
            if (init != 0) prev = init - 8;
            var next = 8;
            var nextLast = 8;
            if (next != 8) next = last + 8;
            prevLast = prev + 8;
            nextLast = next + 8;

            var inicio = 0;
            var fin = 8;

            cardsContainer.innerHTML = cardsContainer.innerHTML + '<div class="col-12 "/>\n';
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '   <nav>\n' +
                '       <ul class="pagination">\n' +
                '           <li class="page-item"><a  href="javascript:agregarTarjetas(' + prev + ',' + prevLast +');" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span></a></li>\n';
            for (i=1; i <= numPaginas; i++)
            {
                cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetas(' + inicio + ',' + fin +');" class="page-link">' + i + '</a></li>\n';
                inicio= inicio + 8;
                fin = fin + 8;
            }
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetas(' + next + ',' + nextLast +');" class="page-link" aria-label="Next"><span aria-hidden="true">»</span></a></li>\n' +
                '       </ul>\n' +
                '    </nav>\n';
        }
    };

    var agregarTarjetasTodos = function agregarTarjetasTodos(init, last) {
        console.log('agregarTarjetas');
        var cardsContainer = document.getElementById("cardsContainer");
        cardsContainer.innerHTML = "";
        //let institucionesDistinct =  getUnique(todosList,'DatosDeLaInstitucion','nombreinstitucion');

        //Paginación
        let num = todosList.length;
        let res = num /8;
        let numPaginas = Math.ceil(res);


        for (i = 0; i < todosList.length; i++) {
            if (i>= init && i<last)
            {
                var title = todosList[i].val().DatosDeLaInstitucion.nombreinstitucion;
                var description = todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion;
                var image = todosList[i].val().Imagen;
                var calle = todosList[i].val().DatosDeLaInstitucion.calle;
                var nro = todosList[i].val().DatosDeLaInstitucion.numero;
                var provincia = todosList[i].val().DatosDeLaInstitucion.provincia;
                var localidad = todosList[i].val().DatosDeLaInstitucion.distrito;

                image = todosList[i].val().Imagen;
                if (image == "")
                {
                    image = "./img/default.jpg"
                }

                //if (institucionesDistinct[i].add == false) {
                cardsContainer.innerHTML = cardsContainer.innerHTML +
                    '         <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-around"  id="content">\n' +
                    '            <div class="card mt-5 shadow mb-5 bg-white rounded-lg " style="width: 12rem;" id="siteNotice">\n' +
                    '                <img src=' + image + ' class="card-img-top" alt="">\n' +
                    '            <div class="card-body">\n' +
                    '            <small>' + localidad + '</small>\n' +
                    '                <h5 class="card-title"><div id="firstHeading" class="firstHeading">' + todosList[i].val().DatosDeLaInstitucion.nombreinstitucion + '</div></h5>\n' +
                    '            <div id="bodyContent">\n' +
                    '                <p class="card-text " id="bloque"><p>' + todosList[i].val().DatosDeLaInstitucion.descripcioninstitucion.substring(0, 50) + '<br> \n' +
                    "<a href='javascript:openModal(\"" + title + "\",\"" + description + "\",\"" + image + "\",\"" + calle + "\",\"" + nro + "\",\"" + provincia + "\");' class='btn btn-outline-none btn-sm mt-2' id='boton'>Mas Info</a>" + '\n' +
                    '            </div>\n' +
                    '            </div>\n' +
                    '            </div>\n' +
                    '        </div> \n'
                ;

                //institucionesDistinct[i].add = true;
                //}
            }
        }
        if (numPaginas > 1)
        {
            var prev = 0;
            var prevLast = 0;
            if (init != 0) prev = init - 8;
            var next = 8;
            var nextLast = 8;
            if (next != 8) next = last + 8;
            prevLast = prev + 8;
            nextLast = next + 8;

            var inicio = 0;
            var fin = 8;

            cardsContainer.innerHTML = cardsContainer.innerHTML + '<div class="col-12 "/>\n';
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '   <nav>\n' +
                '       <ul class="pagination">\n' +
                '           <li class="page-item"><a  href="javascript:agregarTarjetasTodos(' + prev + ',' + prevLast +');" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span></a></li>\n';
            for (i=1; i <= numPaginas; i++)
            {
                cardsContainer.innerHTML = cardsContainer.innerHTML +
                    '           <li class="page-item"><a href="javascript:agregarTarjetasTodos(' + inicio + ',' + fin +');" class="page-link">' + i + '</a></li>\n';
                inicio= inicio + 8;
                fin = fin + 8;
            }
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetasTodos(' + next + ',' + nextLast +');" class="page-link" aria-label="Next"><span aria-hidden="true">»</span></a></li>\n' +
                '       </ul>\n' +
                '    </nav>\n';
        }
    };


    function addMarker(lat, lng, title, description, image, calle, nro, provincia, institucionVinculada) {
        console.log("addMarker: ");
        console.log("lat: " + lat);
        console.log("lng: " + lng);
        console.log("title: " + title);
        console.log("description: " + description);
        console.log("Institución Vinculada: " + institucionVinculada);
        let Organizacion = '';

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            title: title,
            icon: 'img/IconComerBien03.png',
        });

        if (image == "")
        {
            image = "./img/default.jpg"
        }

        if (institucionVinculada == null || institucionVinculada == 'undefined')
        {
            Organizacion = "";
        }
        else
        {
            Organizacion = 'Organización: ' + institucionVinculada;
        }

        var contentString =
            '        <div id="siteNotice ">\n' +
            '            <div class="card row no-gutters shadow bg-white rounded-lg overflow-hidden" style=" max-height: 240px;">\n' +
            '               <div class="row no-gutters">\n' +
            '                   <div class="col-md-4">\n' +
            '                <img src="' + image + '" class="card-img" alt="">\n' +
            '                   </div>\n' +
            '               <div class="col-md-8 ">\n' +
            '                <div class="card-body">\n' +
            '                    <h5 class="card-title">\n' +
            '                        <div id="firstHeading" class="firstHeading">' + title + '</div>\n' +
            '                    </h5>\n' +
            '                    <div id="bodyContent">\n' +
            '                        <p class="card-text" id="bloque" >\n' +
            '                        <p class="overflow-hidden"><b></b> Provincia de ' + provincia + ', con dirección:' + calle + ' ' + nro + '\n' + Organizacion + '\n' +
            '                    </div>\n' +
            '                </div>\n' +
            "<a href='javascript:openModal(\"" + title + "\",\"" + description + "\",\"" + image + "\",\"" + calle + "\",\"" + nro + "\",\"" + provincia + "\");' class='btn btn-outline-none btn-sm mt-2' id='boton'>Mas Info</a>" + '\n' +
            '              </div>\n' +
            '               </div>\n' +
            '             </div>\n' +
            '          </div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString,

        });


        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        markers.push(marker);
    }

    function deleteAllMarkers() {
        //Loop through all the markers and remove
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    };

    function moveCamera(lat, lng, zoom) {
        if (zoom == null) zoom = 13;
        console.log("move camera ");
        console.log("lat: " + lat);
        console.log("lng: " + lng);
        if (lat == null && lng == null) {
            console.log("move camera returning...");
            return;
        }
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: {lat: lat, lng: lng},
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            fullscreenControl: true
        });

    }

    function selectElement(id, valueToSelect) {
        var element = document.getElementById(id);
        element.selectedIndex = valueToSelect;
        // element.value = valueToSelect;
    }


</script>
<script>

    function loadTodos(provinciaId, partidoId, servicios){
        console.log("loadTodos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId;
        console.log("ref: " + reference);

        if (servicios === "merenderoy comedor") {

            database
                .ref(reference)
                .once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function (childSnapshot) {
                        //console.log(childSnapshot.key);
                        //console.log(childSnapshot.val());
                        childSnapshot.forEach(function (childSnapshots) {
                            addMarker(
                                childSnapshots.val().latitude,
                                childSnapshots.val().longitude,
                                childSnapshots.val().DatosDeLaInstitucion.nombreinstitucion,
                                childSnapshots.val().DatosDeLaInstitucion.descripcioninstitucion,
                                childSnapshots.val().Imagen,
                                childSnapshots.val().DatosDeLaInstitucion.calle,
                                childSnapshots.val().DatosDeLaInstitucion.numero,
                                childSnapshots.val().DatosDeLaInstitucion.provincia
                            );

                            var obj = childSnapshots.val();
                            obj.add = false;
                            institucionesList.push(obj);
                        })
                    });
                })
                .then(function (tarjetas) {
                    agregarTarjetas(0, 8);
                });
        }
        else if (servicios === 'todos')
        {
            database
                .ref(reference)
                .once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function (childSnapshot) {
                        //console.log(childSnapshot.key);
                        //console.log(childSnapshot.val());
                        childSnapshot.forEach(function (childSnapshots) {
                            addMarker(
                                childSnapshots.val().latitude,
                                childSnapshots.val().longitude,
                                childSnapshots.val().DatosDeLaInstitucion.nombreinstitucion,
                                childSnapshots.val().DatosDeLaInstitucion.descripcioninstitucion,
                                childSnapshots.val().Imagen,
                                childSnapshots.val().DatosDeLaInstitucion.calle,
                                childSnapshots.val().DatosDeLaInstitucion.numero,
                                childSnapshots.val().DatosDeLaInstitucion.provincia
                            );

                            var obj = childSnapshots.val();
                            obj.add = false;
                            institucionesList.push(obj);
                        })
                    });
                })
                .then(function (tarjetas) {
                    agregarTarjetasTodos(0, 8);
                });
        }
    }


    function loadMerenderos(provinciaId, partidoId) {
        console.log("loadMerenderos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId + "/merendero";
        console.log("ref: " + reference);

        database
            .ref(reference)
            .once('value')
            .then(function (snapshot) {
                //console.log(snapshot.key);
                snapshot.forEach(function (childSnapshot) {
                    //console.log(childSnapshot.key);
                    //console.log(childSnapshot.val());


                    addMarker(
                        childSnapshot.val().latitude,
                        childSnapshot.val().longitude,
                        childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion,
                        childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion,
                        childSnapshot.val().Imagen,
                        childSnapshot.val().DatosDeLaInstitucion.calle,
                        childSnapshot.val().DatosDeLaInstitucion.numero,
                        childSnapshot.val().DatosDeLaInstitucion.provincia
                    );

                    var obj = childSnapshot.val();
                    obj.add = false;
                    institucionesList.push(obj);
                });
            })
            .then(function (tarjetas) {
                agregarTarjetas(0, 8);
            });

    }

    function loadComederos(provinciaId, partidoId) {
        console.log("loadComederos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId + "/comederos";
        console.log("ref: " + reference);


        database
            .ref(reference)
            .once('value')
            .then(function (snapshot) {
                //console.log(snapshot.key);
                snapshot.forEach(function (childSnapshot) {
                    //console.log(childSnapshot.key);
                    console.log(childSnapshot.val());

                    addMarker(
                        childSnapshot.val().latitude,
                        childSnapshot.val().longitude,
                        childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion,
                        childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion,
                        childSnapshot.val().Imagen,
                        childSnapshot.val().DatosDeLaInstitucion.calle,
                        childSnapshot.val().DatosDeLaInstitucion.numero,
                        childSnapshot.val().DatosDeLaInstitucion.provincia,
                        childSnapshot.val().InstitucionVinculada.descripcion
                    );

                    var obj = childSnapshot.val();
                    obj.add = false;
                    institucionesList.push(obj);

                });
            })
            .then(function (tarjetas) {
                agregarTarjetas(0, 8);
            });

    }

    var clearSelect = function removeOptions(documentId) {
        selectbox = document.getElementById(documentId);
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    switch (document.getElementById("serviciosList").value)
    {
        case "Merendero y Comedor":
            loadTodos();
            break;
        case "Todos":
            loadTodos();
            break;
        case "Merenderos":
            loadMerenderos();
            break;
        case "Comedores":
            loadComederos();
            break;
    }

    function cargarMarcas(provinciaId, municipioId) {

        database
            .ref("provincias/" + provinciaId + "/partidos/" + municipioId)
            .once('value')
            .then(function (snapshot) {
                moveCamera(snapshot.val().latitude, snapshot.val().longitude, snapshot.val().zoom);

                var valServ = comboServicios.options[comboServicios.selectedIndex].text.toLowerCase().trim().replace(' ', '');

                console.log("valServ: " + valServ);
                // console.log("valEnerg: " + valEnerg);

                if (valServ === "merenderoy comedor") {
                    console.log("camino 1")
                    loadTodos(provinciaId, municipioId, valServ);

                } else if (valServ === "merenderos") {
                    console.log("camino 2");
                    loadMerenderos(provinciaId, municipioId);

                    console.log("camino 3");
                } else if (valServ === "comedores") {
                    loadComederos(provinciaId, municipioId);

                }
                else if (valServ === "todos") {
                    loadTodos(provinciaId, municipioId, valServ);

                }
            });

    }

</script>
<script>
    // Get the modal
    var modal = document.getElementById('myModal');
    var span = document.getElementsByClassName("close")[0];

    span.onclick = function () {
        modal.style.display = "none";
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    var openModal = function (title, description, image, calle, nro, provincia) {
        console.log('openModal ');

        var content = document.getElementById('content-dialog');

        content.innerHTML =
            '        <div id="siteNotice ">\n' +
            '            <div class="card row no-gutters shadow bg-white rounded-lg overflow-hidden" style=" max-height: 240px;">\n' +
            '               <div class="row no-gutters">\n' +
            '                   <div class="col-md-4">\n' +
            '                <img src="' + image + '" class="card-img" alt="">\n' +
            '                   </div>\n' +
            '               <div class="col-md-8 ">\n' +
            '                <div class="card-body" style="height: 250px;overflow-y:scroll;">\n' +
            '                    <h5 class="card-title">\n' +
            '                        <div id="firstHeading" class="firstHeading">' + title + '</div>\n' +
            '                    </h5>\n' +
            '                    <div id="bodyContent" >\n' +
            '                        <p class="card-text" id="bloque" >\n' +
            '                        <p class="overflow-hidden"><b></b> ' + description + '\n' +
            '                <p class="card-text " id="bloque"><p><b>' + 'Provincia de:' + provincia + ',' + calle + ' ' + nro + '</b><br> \n' +
            '                    </div>\n' +
            '                </div>\n' +
            '              </div>\n' +
            '               </div>\n' +
            '             </div>\n' +
            '          </div>';


        modal.style.display = "block";


    }
</script>

</body>
</html>





















