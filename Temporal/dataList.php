<!DOCTYPE html >
<head>
<!--    <% php header("Content-Type: text/html; charset=iso-8859-1") %>    -->
    <meta charset="utf-8">
    <!--<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>-->
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="liststyle.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">
    <title>Registros de Merenderos y Comederos</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

        /* Optional: Makes the sample page fill the window. */

    </style>
</head>

<html>
<body>

<div class="row ml-5 mr-5 mt-5 justify-content-arroun" >
    <h2>Listado de comederos y merenderos</h2>
</div>
<div class="row ml-5 mr-5 mt-5 input-group mb-2 mt-2">
    <div class="input-group-append">
        <h3>Exportación de datos</h3>
    </div>
    <div class="col-6 input-group-append">
        <label for="exportación"></label>
        <input id="Exportar" type="button" class="input-group-text" value="Exportar" onclick="exportar();"/>
    </div>
</div>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase.js"></script>
<script>
    // Initialize Firebase
    console.log('Initialize Firebase');
    var config = {
        apiKey: "AIzaSyBwfELRbz6TWmqEQAjKWI4u7aQ9V8FL8Nw",
        authDomain: "comer-bien.firebaseapp.com",
        databaseURL: "https://comer-bien.firebaseio.com",
        projectId: "comer-bien",
        storageBucket: "comer-bien.appspot.com",
        messagingSenderId: "471498877486"
    }
    firebase.initializeApp(config);
</script>
<script>
    var database = firebase.database();
    var merenderosList = [];
    var comederosList = [];
    var exportarList = [];


</script>
<script>
    function deleteRow(row) {
        console.log("deleteRow");
        var index = row.parentNode.parentNode.rowIndex;
        var key = row.parentNode.parentNode.cells[0].innerHTML;
        var provincia = row.parentNode.parentNode.cells[1].innerHTML;
        var municipio = row.parentNode.parentNode.cells[2].innerHTML;
        var tipo = row.parentNode.parentNode.cells[3].innerHTML;

        console.log(" " + key);
        //document.getElementById('table').deleteRow(index);

        var reference = "provincias/" + provincia + "/partidos/" + municipio;

        if (tipo === 'merendero') {
            reference = reference + "/merendero/" + key;
        } else if (tipo === 'comedor') {
            reference = reference + "/comederos/" + key;
        }

        console.log("iniciando eliminacion de " + key);
        console.log(reference);

        try {
            database
                .ref(reference)
                .remove()
                .then(function () {
                    console.log("Remove succeeded. " + key)
                    document.getElementById('table').deleteRow(index);
                    alert('Eliminado correctamente');
                })
                .catch(function (error) {
                    console.log("Remove failed: " + key + " " + error.message)
                    alert(error.message);
                });
        } catch (e) {
            console.log(e);
        }
    }

    function exportar() {

        database
            .ref('provincias')
            .once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (provinciasSnapshot) {

                    provinciasSnapshot.forEach(function (municipiosSnapshot) {

                        municipiosSnapshot.forEach(function (childSnapshot) {
                            //console.log(childSnapshot.key);
                            childSnapshot.child("merendero").forEach(function (merenderosSnapshot) {
                                merenderosSnapshot.province = provinciasSnapshot.key;
                                merenderosSnapshot.department = childSnapshot.key;
                                merenderosSnapshot.type = 'merendero';
                                exportarList.push(merenderosSnapshot);
                            });
                            childSnapshot.child("comederos").forEach(function (comederosSnapshot) {
                                comederosSnapshot.province = provinciasSnapshot.key;
                                comederosSnapshot.department = childSnapshot.key;
                                comederosSnapshot.type = 'comedor';
                                exportarList.push(comederosSnapshot);
                            })

                        })

                    })


                });

            })
            .then(function (listado) {
                exportarListado();
            });
    }

    function exportarListado()
    {
        var cabecera = null;
        if (cabecera == null)
        {
            cabecera = "Nombre de Institución;" +
                          "Descripción de Institución;" +
                          "Provincia;" +
                          "Localidad;" +
                          "Calle;" +
                          "Número;" +
                          "Barrio;" +
                          "Entre calles;" +
                          "Año de inicio de actividad;" +
                          "Mes de inicio de actividad;" +
                          "Tiene institución vinculada;" +
                          "Nombre de la institución vinculada;" +
                          "Realiza otras actividqades;" +
                          "Actividades Culturales;" +
                          "Actividades Deportivas;" +
                          "Talleres de Oficios;" +
                          "Terminalidad Educativa;" +
                          "Apoyo;" +
                          "Otras Actividades;" +
                          "Domingo;" +
                          "Hora;" +
                          "Lunes;" +
                          "Hora;" +
                          "Martes;" +
                          "Hora;" +
                          "Miercoles;" +
                          "Hora;" +
                          "Jueves;" +
                          "Hora;" +
                          "Viernes;" +
                          "Hora;" +
                          "Sabado;" +
                          "Hora;" +
                          "Servicio que ofrece;" +
                          "Cantidad de Asistentes;" +
                          "Cantidad de Comensales;" +
                          "Tipo de energia que utiliza;" +
                          "Nombre del responsalbe;" +
                          "Email del responsable;" +
                          "Telefono del responsable;" +
                          "Imagen de la institución;" +
                          "Latitud;" +
                          "Longitud;";
        }
        var rows = null;
        if (rows == null)
        {
            rows="";
            for (i = 0; i < exportarList.length; i++)
            {
                try {
                    fila =
                        exportarList[i].val().DatosDeLaInstitucion.nombreinstitucion.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DatosDeLaInstitucion.descripcioninstitucion.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DatosDeLaInstitucion.provincia.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DatosDeLaInstitucion.distrito.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DatosDeLaInstitucion.calle.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DatosDeLaInstitucion.numero.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DatosDeLaInstitucion.barrio.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DatosDeLaInstitucion.entrecalle.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().InicioDeActividad.año.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().InicioDeActividad.mes.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().InstitucionVinculada.vinculada.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().InstitucionVinculada.descripcion.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().OtrasActividades.otras.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().OtrasActividades.descripcion.ActividadesCulturales + ';' +
                        exportarList[i].val().OtrasActividades.descripcion.ActividadesDeportivas + ';' +
                        exportarList[i].val().OtrasActividades.descripcion.TalleresOficios + ';' +
                        exportarList[i].val().OtrasActividades.descripcion.TerminalidadEducativa + ';' +
                        exportarList[i].val().OtrasActividades.descripcion.apoyo + ';' +
                        exportarList[i].val().OtrasActividades.descripcion.otras + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.domingo.dia + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.domingo.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.lunes.dia + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.lunes.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.martes.dia + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.martes.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.miercoles.dia + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.miercoles.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.jueves.dia + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.jueves.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.viernes.dia + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.viernes.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.sabado.dia + ';' +
                        exportarList[i].val().DiasDeFuncionamiento.sabado.hora.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().ServicioQueOfrece.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().CantidadDeAsistentes.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().CantidadDeComensales.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().TipoDeEnergia.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().Responsable.nombre.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().Responsable.email.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().Responsable.telefono.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().Imagen.replace(/(\r\n|\n|\r|;)/gm, "") + ';' +
                        exportarList[i].val().latitude + ';' +
                        exportarList[i].val().longitude + ';' +
                        exportarList[i].key.replace(/(\r\n|\n|\r|;)/gm, "") + ';' + '\n';
                    rows = rows + fila;
                }
                catch (e) {
                    console.log(e.toString());
                }

            }
        }
        var CSV = cabecera + '\n' + rows;

        let encoding = "data:text/csv;charset=utf-8,%EF%BB%BF" + CSV;
        var encodedUri = encodeURI(encoding);
        var universalBOM = "\uFEFF";
        var link = document.createElement("a");
        link.setAttribute("href", 'data:text/csv; charset=utf-8,' + encodeURIComponent(universalBOM+CSV));
        link.setAttribute("download", "exportado.csv");
        document.body.appendChild(link); // Required for FF
        link.click();


    }

    var populateTable = function tableCreate() {
        console.log("populate table");
        //body reference
        var body = document.getElementsByTagName("body")[0];

        // create elements <table> and a <tbody>
        var tbl = document.createElement("table");
        tbl.id = "table";
        var tblBody = document.createElement("tbody");

        var heading = new Array();
        // heading[0] = "Nombre"
        // heading[1] = "Provincia"
        // heading[2] = "Municipio"
        // heading[3] = "Tipo"
        // heading[4] = " "

        //heading[3] = "Quantity"

        heading[0] = "Nombre de Institución";
        heading[1] = "Descripción de Institución";
        heading[2] = "Provincia";
        heading[3] = "Localidad";
        heading[4] = "Calle";
        heading[5] = "Número";
        heading[6] = "Barrio";
        heading[7] = "Entre calles";
        heading[8] = "Año de inicio de actividad";
        heading[9] = "Mes de inicio de actividad";
        heading[10] = "Tiene institución vinculada";
        heading[11] = "Nombre de la institución vinculada";
        heading[12] = "Realiza otras actividqades";
        heading[13] = "Actividades Culturales";
        heading[14] = "Actividades Deportivas";
        heading[15] = "Talleres de Oficios";
        heading[16] = "Terminalidad Educativa";
        heading[17] = "Apoyo";
        heading[18] = "Otras Actividades";
        heading[19] = "Domingo";
        heading[20] = "Hora";
        heading[21] = "Lunes";
        heading[22] = "Hora";
        heading[23] = "Martes";
        heading[24] = "Hora";
        heading[25] = "Miercoles";
        heading[26] = "Hora";
        heading[27] = "Jueves";
        heading[28] = "Hora";
        heading[29] = "Viernes";
        heading[30] = "Hora";
        heading[31] = "Sabado";
        heading[32] = "Hora";
        heading[33] = "Servicio que ofrece";
        heading[34] = "Cantidad de Asistentes";
        heading[35] = "Cantidad de Comensales";
        heading[36] = "Tipo de energia que utiliza";
        heading[37] = "Nombre del responsalbe";
        heading[38] = "Email del responsable";
        heading[39] = "Telefono del responsable";
        heading[40] = "Imagen de la institución";
        heading[41] = "Latitud";
        heading[42] = "Longitud";
        heading[43] = " ";

        var tr = document.createElement('TR');
        tblBody.appendChild(tr);
        for (i = 0; i < heading.length; i++) {
            var th = document.createElement('TH')
            th.width = '75';
            th.appendChild(document.createTextNode(heading[i]));
            tr.appendChild(th);
        }

        for (i = 0; i < comederosList.length; i++) {
            var tr = document.createElement('TR');

            // var td = document.createElement('TD')
            // td.appendChild(document.createTextNode(comederosList[i].key));
            // tr.appendChild(td)
            //
            // var td = document.createElement('TD')
            // td.appendChild(document.createTextNode(comederosList[i].province));
            // tr.appendChild(td)
            //
            // var td = document.createElement('TD')
            // td.appendChild(document.createTextNode(comederosList[i].department));
            // tr.appendChild(td)
            //
            // var td = document.createElement('TD')
            // td.appendChild(document.createTextNode(comederosList[i].type));
            // tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().DatosDeLaInstitucion.nombreinstitucion));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().DatosDeLaInstitucion.descripcioninstitucion));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().DatosDeLaInstitucion.provincia));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().DatosDeLaInstitucion.distrito));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().DatosDeLaInstitucion.calle));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().DatosDeLaInstitucion.numero));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().DatosDeLaInstitucion.barrio));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().DatosDeLaInstitucion.entrecalle));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().InicioDeActividad.año));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().InicioDeActividad.mes));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().InstitucionVinculada.vinculada));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().InstitucionVinculada.descripcion));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().OtrasActividades.otras));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().OtrasActividades.descripcion.ActividadesCulturales));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().OtrasActividades.descripcion.ActividadesDeportivas));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().OtrasActividades.descripcion.TalleresOficios));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().OtrasActividades.descripcion.TerminalidadEducativa));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().OtrasActividades.descripcion.apoyo));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().OtrasActividades.descripcion.otras));
            tr.appendChild(td)

            try
            {
                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.domingo.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.domingo.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.lunes.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.lunes.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.martes.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.martes.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.miercoles.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.miercoles.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.jueves.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.jueves.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.viernes.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.viernes.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.sabado.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(comederosList[i].val().DiasDeFuncionamiento.sabado.hora));
                tr.appendChild(td)
            }
            catch (e) {

            }

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().ServicioQueOfrece));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().CantidadDeAsistentes));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().CantidadDeComensales));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().TipoDeEnergia));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().Responsable.nombre));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().Responsable.email));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().Responsable.telefono));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().Imagen));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().latitude));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(comederosList[i].val().longitude));
            tr.appendChild(td)

            var button = document.createElement("BUTTON");
            button.innerHTML = " . ";
            button.addEventListener("click", function () {
                //alert(this);
                deleteRow(this);
            });

            var td = document.createElement('TD')
            td.appendChild(document.body.appendChild(button));
            tr.appendChild(td)
            tr.className += "comedor";

            tblBody.appendChild(tr);
        }


        for (i = 0; i < merenderosList.length; i++) {
            var tr = document.createElement('TR');
            var td = document.createElement('TD')
            // td.appendChild(document.createTextNode(merenderosList[i].key));
            // tr.appendChild(td)
            //
            // var td = document.createElement('TD')
            // td.appendChild(document.createTextNode(merenderosList[i].province));
            // tr.appendChild(td)
            //
            // var td = document.createElement('TD')
            // td.appendChild(document.createTextNode(merenderosList[i].department));
            // tr.appendChild(td)
            //
            // var td = document.createElement('TD')
            // td.appendChild(document.createTextNode(merenderosList[i].type));
            // tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().DatosDeLaInstitucion.nombreinstitucion));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().DatosDeLaInstitucion.descripcioninstitucion));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().DatosDeLaInstitucion.provincia));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().DatosDeLaInstitucion.distrito));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().DatosDeLaInstitucion.calle));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().DatosDeLaInstitucion.numero));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().DatosDeLaInstitucion.barrio));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().DatosDeLaInstitucion.entrecalle));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().InicioDeActividad.año));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().InicioDeActividad.mes));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().InstitucionVinculada.vinculada));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().InstitucionVinculada.descripcion));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().OtrasActividades.otras));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().OtrasActividades.descripcion.ActividadesCulturales));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().OtrasActividades.descripcion.ActividadesDeportivas));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().OtrasActividades.descripcion.TalleresOficios));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().OtrasActividades.descripcion.TerminalidadEducativa));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().OtrasActividades.descripcion.apoyo));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().OtrasActividades.descripcion.otras));
            tr.appendChild(td)

            try
            {
                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.domingo.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.domingo.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.lunes.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.lunes.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.martes.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.martes.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.miercoles.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.miercoles.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.jueves.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.jueves.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.viernes.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.viernes.hora));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.sabado.dia));
                tr.appendChild(td)

                var td = document.createElement('TD')
                td.appendChild(document.createTextNode(merenderosList[i].val().DiasDeFuncionamiento.sabado.hora));
                tr.appendChild(td)
            }
            catch (e) {

            }

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().ServicioQueOfrece));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().CantidadDeAsistentes));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().CantidadDeComensales));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().TipoDeEnergia));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().Responsable.nombre));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().Responsable.email));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().Responsable.telefono));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().Imagen));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().latitude));
            tr.appendChild(td)

            var td = document.createElement('TD')
            td.appendChild(document.createTextNode(merenderosList[i].val().longitude));
            tr.appendChild(td)

            var button = document.createElement("BUTTON");
            button.innerHTML = " . ";
            button.addEventListener("click", function () {
                //alert(this);
                deleteRow(this);
            });

            var td = document.createElement('TD')
            td.appendChild(document.body.appendChild(button));
            tr.appendChild(td)
            tr.className += "merendero";


            tblBody.appendChild(tr);
        }

        // append the <tbody> inside the <table>
        tbl.appendChild(tblBody);
        // put <table> in the <body>
        body.appendChild(tbl);
        // tbl border attribute to
        tbl.setAttribute("border", "2");
    }


    var descargarDatos = function descargarDatos() {

        database
            .ref('provincias')
            .once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (provinciasSnapshot) {
                    //console.log(provinciasSnapshot.key);

                    provinciasSnapshot.forEach(function (municipiosSnapshot) {

                        municipiosSnapshot.forEach(function (childSnapshot) {
                            //console.log(childSnapshot.key);

                            childSnapshot.child("merendero").forEach(function (merenderosSnapshot) {

                                merenderosSnapshot.province = provinciasSnapshot.key;
                                merenderosSnapshot.department = childSnapshot.key;
                                merenderosSnapshot.type = 'merendero';
                                //console.log(merenderosSnapshot.key);
                                merenderosList.push(merenderosSnapshot);

                            });

                            childSnapshot.child("comederos").forEach(function (comederosSnapshot) {

                                comederosSnapshot.province = provinciasSnapshot.key;
                                comederosSnapshot.department = childSnapshot.key;
                                comederosSnapshot.type = 'comedor';
                                //console.log(comederosSnapshot.key);
                                comederosList.push(comederosSnapshot);
                            })

                        })

                    })


                })

                console.log("comedores registrados: " + comederosList.length);
                console.log("merenderos registrados: " + merenderosList.length);
                console.log("fin");

                populateTable();
            })
    }

    descargarDatos();

</script>


</body>
</html>
