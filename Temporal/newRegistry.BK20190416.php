<!DOCTYPE html >

<head xmlns="http://www.w3.org/1999/html">
    <meta charset="utf-8">
    <link rel="stylesheet" href="eldiseño.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
     integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">
</head>
<html>
<body>
<!-- Iniciación Firebase -->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0&libraries=places&callback=initAutocomplete" async defer></script>-->
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="waitingfor.js"></script>
<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0"></script>-->
<script>
    // Initialize Firebase
    console.log('Initialize Firebase');

    var config = {
        apiKey: "AIzaSyBwfELRbz6TWmqEQAjKWI4u7aQ9V8FL8Nw",
        authDomain: "comer-bien.firebaseapp.com",
        databaseURL: "https://comer-bien.firebaseio.com",
        projectId: "comer-bien",
        storageBucket: "comer-bien.appspot.com",
        messagingSenderId: "471498877486"
    }

    var geocoder = null;
    var adress = null;


    firebase.initializeApp(config);

    var placeSearch, autocomplete;
    var geolocation;
    var database = firebase.database();

    var comboMunicipios;// = document.getElementById("selectDistritos");
    var comboProvincias;// = document.getElementById("selectProvincia");

    var lat;
    var lng;

    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        administrative_area_level_2: 'short_name',
        country: 'long_name',
        postal_code: 'short_name',
        postal_code_suffix: 'short_name'
    };

    function initAutocomplete() {

        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('calle'), {types: ['geocode']});
        autocomplete.setFields(['address_component']);
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {

        var place = autocomplete.getPlace();

        // for (var component in componentForm) {
        //     document.getElementById(component).value = '';
        //     document.getElementById(component).disabled = false;
        // }

        // lat = autocomplete.bounds.ma.l;
        // lng = autocomplete.bounds.ga.l;

        adress = document.getElementById('calle').value;
        getLatitudeLongitude(showResult, adress)

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var val;
            if (addressType == 'street_number') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('numero').value = val;
            }
            if (addressType == 'route') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('calle').value = val;
            }
            if (addressType == 'locality') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('selectDistritos').value = val;
            }
            if (addressType == 'administrative_area_level_1') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('selectProvincia').value = val;
            }
            // if (addressType == 'administrative_area_level_2') {
            //     val = place.address_components[i][componentForm[addressType]];
            //     document.getElementById('selectDistritos').value = val;
            // }
            // if (addressType == 'country') {
            //     val = place.address_components[i][componentForm[addressType]];
            //     document.getElementById(addressType).value = val;
            // }
            // if (addressType == 'postal_code') {
            //     val = place.address_components[i][componentForm[addressType]];
            //     document.getElementById(addressType).value = val;
            // }
            // if (addressType == 'postal_code_suffix') {
            //     val = place.address_components[i][componentForm[addressType]];
            //     document.getElementById(addressType).value = val;
            // }
        }
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle(
                    {center: geolocation, radius: position.coords.accuracy});
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    function showResult(result) {
        lat = result.geometry.location.lat();
        lng = result.geometry.location.lng();
    }

    function getLatitudeLongitude(callback, address) {
        // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
        address = address || 'Ferrol, Galicia, Spain';
        // Initialize the Geocoder
        geocoder = new google.maps.Geocoder();
        if (geocoder) {
            geocoder.geocode({
                'address': address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    callback(results[0]);
                }
            });
        }
    }

    $(document).ready(function(){
        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById('lblimage').firstChild.data = fileName;
            alert('El archivo "' + fileName +  '" ha sido seleccionado.');
        });
    });

    window.onload = function() {

        geocoder = new google.maps.Geocoder();

        var nombreInstitucion = document.getElementById('nombreinstitucion').value;
        var imagen = document.getElementById('image').value;

        comboMunicipios = document.getElementById("selectDistritos");
        comboProvincias = document.getElementById("selectProvincia");
        // loadProv();

        document.getElementById("Subir").onclick = function upload()
        {
            const ref = firebase.storage().ref();
            const file = document.querySelector('#image').files[0]
            const name = (+new Date()) + '-' + file.name;
            const metadata = {
                contentType: file.type
            };
            const task = ref.child(name).put(file, metadata);

            waitingDialog.show("Cargando el archivo");

            task
                .then(snapshot => snapshot.ref.getDownloadURL())
                .then((url) => {
                    console.log(url);
                    //document.querySelector('#image').src = url;
                    imagen = url;
                    waitingDialog.hide();
                    //window.alert("Se ha subido correctamente la foto.")
                })
                .catch(console.error);
        };

        document.getElementById('submitFirebase').onclick = function () {

            //Creación de variables
            var provincia = document.getElementById('selectProvincia').value;
            var distrito = document.getElementById('selectDistritos').value;
            var barrio = document.getElementById('barrio').value;
            var calle = document.getElementById('calle').value;
            var numero = document.getElementById('numero').value;
            var entrecalle = document.getElementById('entrecalle').value;
            if (nombreInstitucion == null || nombreInstitucion == "")
            {
                nombreInstitucion = document.getElementById('nombreinstitucion').value;
            }
            var descripcioninstitucion = document.getElementById('descripcioninstitucion').value;
            var dia = document.getElementById('Dia').value;
            var hora = document.getElementById('hora').value;
            var servicioqueofrece = document.getElementById('servicioqueofrece').value;
            var anio = document.getElementById('anio').value;
            var mes = document.getElementById('mes').value;
            var vinculo = document.getElementById('vinculo').value;
            var explicacion = document.getElementById('explicacion').value;
            var asistentes = document.getElementById('asistentes').value;
            var comensales = document.getElementById('comensales').value;
            var tipoenergia = document.getElementById('tipoenergia').value;
            var otrasactividades = document.getElementById('otrasactividades').value;
            var apoyo = document.getElementById('apoyo').value;
            var terminalidadeducativa = document.getElementById('TerminalidadEducativa').value;
            var actividadesculturales = document.getElementById('ActividadesCulturales').value;
            var talleresoficios = document.getElementById('TalleresOficios').value;
            var actividadesdeportivas = document.getElementById('ActividadesDeportivas').value;
            var cuales = document.getElementById('cuales').value;
            var nombreresponsable = document.getElementById('nombreresponsable').value;
            var telefonoresponsable = document.getElementById('telefonoresponsable').value;
            var mailresponsable = document.getElementById('mailresponsable').value;

            var servicioPath=[];

            if (provincia != "" && distrito != ""  )
            {
                if (servicioqueofrece == 'merendero' || servicioqueofrece == 'comederos')
                {
                    servicioPath[0] = servicioqueofrece;
                }
                else if (servicioqueofrece != "Servicio Que Ofrece")
                {
                    servicioPath[0] = 'merendero';
                    servicioPath[1] = 'comederos';
                }

                //Comienzo de insercion de datos en firebase
                var i;
                for (i = 0; i<servicioPath.length; i++)
                {
                    firebase.database().ref('provincias/' + provincia + '/partidos/' + distrito + '/' + servicioPath[i] + '/' + nombreInstitucion + '/').set(
                        {
                            latitude: lat,
                            longitude: lng,
                            DatosDeLaInstitucion:
                                {
                                    provincia: provincia,
                                    distrito: distrito,
                                    barrio: barrio,
                                    calle: calle,
                                    numero: numero,
                                    entrecalle: entrecalle,
                                    nombreinstitucion: nombreInstitucion,
                                    descripcioninstitucion: descripcioninstitucion
                                },
                            DiasDeFuncionamiento:
                                {
                                    dias: dia,
                                    hora: hora
                                },
                            ServicioQueOfrece: servicioPath[i],
                            InicioDeActividad:
                                {
                                    año: anio,
                                    mes: mes
                                },
                            InstitucionVinculada:
                                {
                                    vinculada: vinculo,
                                    descripcion: explicacion
                                },
                            CantidadDeAsistentes: asistentes,
                            CantidadDeComensales: comensales,
                            TipoDeEnergia: tipoenergia,
                            OtrasActividades:
                                {
                                    otras: otrasactividades,
                                    descripcion:
                                        {
                                            apoyo: apoyo,
                                            TerminalidadEducativa: terminalidadeducativa,
                                            ActividadesCulturales: actividadesculturales,
                                            TalleresOficios: talleresoficios,
                                            ActividadesDeportivas: actividadesdeportivas,
                                            otras: cuales
                                        }
                                },
                            Responsable:
                                {
                                    nombre: nombreresponsable,
                                    telefono: telefonoresponsable,
                                    email: mailresponsable
                                },
                            Imagen: imagen
                        });
                }
                //document.getElementById('setFirebaseData').submit();
            }
            else
            {
                window.alert("Por favor revise que se encuentren rellenados Provincia y Distrito.")
            }
            return true;
        };
    };


</script>
<div class="container">
    <div class="row ml-5 mr-5 mt-5 justify-content-arroun" >
        <h2>Formulario para la georeferenciacion de merenderos y comedores</h2>
    </div>

    <form id="setFirebaseData" action="" method="POST">
        <div class="form-group row  justify-content-arround ml-5 mr-5  p-3 rounded" id="fondoa">
            <div class="col-12 ">
                <h5>Datos de la institución</h5>
            </div>
            <div class="col-12 col-md-6">
                <label for="nombreinstitucion"></label>
                <input type="text" name="nombreinstitucion" id="nombreinstitucion" class="form-control"
                       placeholder="Nombre de la Institucion">
            </div>
            <div class="col-12 col-md-6">
                <label for="servicioqueofrece"></label>
                <select id="servicioqueofrece"  class="form-control">
                    <option disabled selected>Servicio Que Ofrece</option>
                    <option value="merendero">Merendero</option>
                    <option value="comederos">Comedor</option>
                    <option value="merenderocomedor">Merendero y Comedor</option>
                </select>
            </div>
            <div class="col-12 mt-3">
                <textarea class="form-control" id="descripcioninstitucion" rows="3" placeholder="Descripción de la Institución"></textarea>
            </div>
            <div class="col-12 mt-3">
                <h6 for="vinculo">Esta vinculado con alguna organización / institución</h6>
            </div>
            <div class="col-3">
                <select id="vinculo"  class="form-control">
                    <option value="si">Si</option>
                    <option value="no">No</option>
                </select>
            </div>
            <div class="col-9">
                <input type="text" name="explicacion" id="explicacion" class="form-control" placeholder="Nombre de la organización/institución">
            </div>
        </div>
        <div class="row  justify-content-arround ml-5 mr-5  p-3 rounded" id="fondob">
            <div class="col-12"><h5>Datos de localización</h5> </div>
            <div class="col-12 col-md-6">
                <label for="calle"></label>
                <input type="text" name="calle" id="calle" class="form-control" onFocus="geolocate()" placeholder="Calle">
            </div>
            <div class="col-6 col-md-2">
<!--            <div class="col-12 col-md-2">-->
                <label for="numero"></label>
                <input type="text" name="numero" id="numero" class="form-control" placeholder="Numero">
            </div>
            <div class="col-6 col-md-3">
                <label for="barrio"></label>
                <input type="text" name="barrio" id="barrio" class="form-control"placeholder="Barrio">
            </div>
            <div class="col-12 col-md-5">
                <label for="selectProvincia"></label>
                <input type="text" name="selectProvincia" id="selectProvincia" class="form-control"placeholder="Provincias" readonly>
<!--                <select id="selectProvincia" class="form-control">-->
<!--                </select>-->
            </div>
            <div class="col-12 col-md-5">
                <label for="selectDistritos"></label>
                <input type="text" name="selectDistritos" id="selectDistritos" class="form-control"placeholder="Distrito" readonly>
<!--                <select id="selectDistritos" class="form-control">-->
<!--                </select>-->
            </div>
            <div class="col-12 col-md-12">
                <label for="entrecalle"></label>
                <input type="text" name="entrecalle" id="entrecalle" class="form-control" placeholder="Entre calle y/o manzana">
            </div>
        </div>
        <div class="row mt-4 justify-content-arround ml-5 mr-5   p-3 rounded" id="fondoc">
            <div class="col-12"><h5>Otros datos importantes</h5></div>
            <div class="col-12"><h6>Dias de funcionamiento y horario</h6></div>
            <div class="col-6 col-md-6">
                <select id="Dia" class="form-control" >
                    <option disabled selected>Dia</option>
                    <option value="Lunes">Lunes</option>
                    <option value="Martes">Martes</option>
                    <option value="Miercoles">Miercoles</option>
                    <option value="Jueves">Jueves</option>
                    <option value="Viernes">Viernes</option>
                    <option value="Sabado">Sabado</option>
                    <option value="Domingo">Domingo</option>
                    <option value="LunesMiercolesViernes">Lunes - Miercoles - Viernes</option>
                    <option value="MartesJueves">Martes - Jueves</option>
                    <option value="LunVie">Luneas a Viernes</option>
                    <option value="TodosDias">Todos los días</option>
                </select>
            </div>
            <div class="col-6 col-md-6">
                <input type="text" name="hora" id="hora"  class="form-control" placeholder="Hora">
            </div>
            <div class="col-12 mt-3">
                <label for="otrasactividades"><h6>Aparte de funcionar como merendero / comedor ¿Se desarrollan otras actividades en el lugar?</h6></label>
            </div>
            <div class="col-12 col-md-12">
                <select id="otrasactividades"  class="form-control">
                    <option value="si">Sí</option>
                    <option value="no">No</option>
                </select>
            </div>
            <div class="col-6 col-md-7 ml-4 mt-3">
                <label for="respuestasi"><h6>Si respondió "Sí", por favor indique cuales (puede elegir más de una)</h6> </label>
                <br/>
                <input type="checkbox" class="form-check-input" name="apoyo" id="apoyo" value="apoyo escolar">Apoyo Escolar</input>
                <br/>
                <input type="checkbox" class="form-check-input" name="TerminalidadEducativa" id="TerminalidadEducativa" value="Programa de terminalidad educativa">Programa de terminalidad educativa</input>
                <br/>
                <input type="checkbox" class="form-check-input" name="ActividadesCulturales" id="ActividadesCulturales" value="Actividades Culturales">Actividades Culturales</input>
                <br/>
                <input type="checkbox" class="form-check-input" name="TalleresOficios" id="TalleresOficios" value="Talleres de oficios">Talleres de oficios</input>
                <br/>
                <input type="checkbox" class="form-check-input" name="ActividadesDeportivas" id="ActividadesDeportivas" value="Actividades deportivas">Actividades deportivas</input>
                <br/>
                <input type="checkbox" class="form-check-input" name="otras" value="Otras">Otras</input>
                <label for="otras"></label>
                <input type="text" name="cuales" id="cuales" class="form-control" placeholder="Cuales" >
            </div>
            <div class="col-12 col-md-12  input-group mb-3 mt-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image" >
                    <label class="custom-file-label" for="image" id="lblimage">Seleccione archivo</label>
                </div>
                <div class="input-group-append">
                    <input id="Subir" type="button" class="input-group-text" value="Subir" onclick="upload();"/>
<!--                    <span class="input-group-text" id="">Subir</span>-->
                </div>
            </div>
        </div>
        <div class="row row mt-4 justify-content-arround ml-5 mr-5   p-3 rounded" id="fondod">
            <div class="col-12"><h5>Datos internos de la red</h5></div>
            <div class="col-12 mt-3"><h6>Menciona el mes en comenzó a funcionar el merendero/ comedor</h6></div>
            <div class="col-6">
                <label for="anio"></label>
                <select id="anio"  class="form-control">
                    <option disabled selected>Año</option>
                    <option value="1990">1990</option>
                    <option value="1991">1991</option>
                    <option value="1992">1992</option>
                    <option value="1993">1993</option>
                    <option value="1994">1994</option>
                    <option value="1995">1995</option>
                    <option value="1996">1996</option>
                    <option value="1997">1997</option>
                    <option value="1998">1998</option>
                    <option value="1999">1999</option>
                    <option value="2000">2000</option>
                    <option value="2001">2001</option>
                    <option value="2002">2002</option>
                    <option value="2003">2003</option>
                    <option value="2004">2004</option>
                    <option value="2005">2005</option>
                    <option value="2006">2006</option>
                    <option value="2007">2007</option>
                    <option value="2008">2008</option>
                    <option value="2009">2009</option>
                    <option value="2010">2010</option>
                    <option value="2011">2011</option>
                    <option value="2012">2012</option>
                    <option value="2013">2013</option>
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                    <option value="2027">2027</option>
                    <option value="2028">2028</option>
                    <option value="2029">2029</option>
                    <option value="2030">2030</option>
                </select>
            </div>
            <div class="col-6">
                <label for="mes"></label>
                <select id = "mes" class="form-control" >
                    <option disabled selected>Mes</option>
                    <option value="enero">Enero</option>
                    <option value="febrero">Febrero</option>
                    <option value="marzo">Marzo</option>
                    <option value="abril">Abril</option>
                    <option value="mayo">Mayo</option>
                    <option value="junio">Junio</option>
                    <option value="julio">Julio</option>
                    <option value="agosto">Agosto</option>
                    <option value="septiembre">Septiembre</option>
                    <option value="octubre">Octubre</option>
                    <option value="noviembre">Noviembre</option>
                    <option value="diciembre">Diciembre</option>
                </select>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <h6 for="asistentes">Cuantas personas trabajan en el merendero / comedor</h6>
                <select id="asistentes"  class="form-control">
                    <option value="1-10">1 y 10</option>
                    <option value="11-20">11 y 20</option>
                    <option value="20+">Mas de 20</option>
                </select>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <h6 for="comensales">Cuantas personas asisten en el merendero / comedor</h6>
                <select id="comensales"  class="form-control">
                    <option value="1-50">1 y 50</option>
                    <option value="51-100">51 y 100</option>
                    <option value="101-50">101 y 50</option>
                    <option value="151-200">151 y 200</option>
                    <option value="200+">Mas de 200</option>
                </select>
            </div>
            <div class="col-12 mt-3">
                <h6 for="tipoenergia">Que tipo de energia se utiliza en el merendero / comedor</h6>
                <select id="tipoenergia"  class="form-control">
                    <option value="natural">Gas Natural</option>
                    <option value="garrafa">Gas envasado (garrafa / tubo / tanque)</option>
                    <option value="lenia">Leña</option>
                    <option value="otro">Otro</option>
                </select>
            </div>
        </div>
        <div class="row justify-content-arround ml-5 mr-5 mt-4 p-3 rounded" id="fondoe">
            <div class="col-12">
                <label for="lbldatosresponsable"><h5>Datos de contacto con responsable del merendero / comedor</h5></label>
            </div>
            <div class="col-6">
                <label for="nombreresponsable"></label>
                <input type="text" name="nombreresponsable" id="nombreresponsable"  class="form-control" placeholder="Nombre">
            </div>
            <div class="col-6">
                <label for="telefonoresponsable"></label>
                <input type="text" name="telefonoresponsable" id="telefonoresponsable"  class="form-control" placeholder="Telefeono">
            </div>
            <div class="col-12">
                <label for="mailresponsable"></label>
                <input type="text" name="temailresponsable" id="mailresponsable"  class="form-control" placeholder="Correo Electronico">
            </div>
        </div>
        <div class="row  mt-5 mb-5 justify-content-center">
            <div class="col-4 text-center  align-self-center">
                <input id="submitFirebase" type="submit" class="button" text="submit">
<!--                <a href="#" id="submitFirebase" class="button">submit</a>-->
            </div>
        </div>
    </form>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0&libraries=places&callback=initAutocomplete" async defer></script>

<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQQUPe-GBmzqn0f8sb_8xZNcseul1N0yU&libraries=places&callback=initAutocomplete" async defer></script>-->
</body>
</html>

