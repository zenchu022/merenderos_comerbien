<!DOCTYPE html >
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="estilos.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">


    <title>Mapa de Comederos y Merenderos</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 450px;
        }


        .DivToScroll{
            background-color: #F5F5F5;
            border: 1px solid #DDDDDD;
            border-radius: 4px 0 4px 0;
            color: #3B3C3E;
            font-size: 12px;
            font-weight: bold;
            left: -1px;
            padding: 10px 7px 5px;
        }

        .DivWithScroll{
            height:120px;
            overflow:scroll;
            overflow-x:hidden;
        }

        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        h5 {
            font-size: 15px;
            border-bottom: 2px solid #f2c94c;
            font-weight: 600;
            color: #949494;

        }

        #bloque {
            margin-top: -15px;
            font-size: 11px;
            position: absolute;
            float: left;
        }

        #boton {
            text-decoration: underline;
            color: #007fee;
            position: relative;
            float: right;
        }

        #boton:hover {
            color: #79c0ff;

        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
            display: block;
        }

        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -ms-overflow-style: scrollbar;
            -webkit-tap-highlight-color: transparent;
        }

        body {
            margin: 0;
            font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }




    </style>
</head>

<body>
<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <div id="content-dialog"></div>
    </div>

</div>
<div class="container">
    <div class="row img-fluid"></div>
    <div class="row shadow shadow p-3 mb-2 bg-white rounded mt-2 justify-content-arround">
        <div id="filterCitys" class="formulario">
            <form>
                <fieldset class="row">
                    <div class="col-9 col-md-12">
                        <h1>
                            <legend>Filtros</legend>
                        </h1>
                        <p>
                            <label>Provincia</label>
                            <select id="provinciasList">
                            </select>
                            <label>Localidad</label>
                            <select id="municipiosList">
                            </select>
<!--                            <label>Energia</label>-->
<!--                            <select id="energiasList">-->
<!--                                <option selected value="todos">Todos</option>-->
<!--                                <option selected value="natural">Gas natural</option>-->
<!--                                <option selected value="garrafa">Garrafa</option>-->
<!--                                <option selected value="lenia">Leña</option>-->
<!--                                <option selected value="otro">Otro</option>-->
<!--                            </select>-->
                            <label>Servicios</label>
                            <select id="serviciosList">
                                <option selected value=0>Merenderos</option>
                                <option selected value=1>Comedores</option>
                                <option selected value=2>Merendero y Comedor</option>
                            </select>
                            <a id='buttonRefrescar' class="btn btn-primary">Refrescar</a>
                        </p>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid" style="padding: 0 !important">
    <div class="row">
        <div id="map" class="col map-container img-fluid shadow p-3 mb-5"></div>
    </div>
</div>

<div class="container">
    
    
    <div id="cardsContainer" class="row" style="height: 500px;overflow-y:scroll;"/></div>
</div>


<script>
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    //var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    /* btn.onclick = function() {
         modal.style.display = "block";
     }
    */
    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    var openModal = function (title, description, image, calle, nro, provincia) {
        console.log('openModal ');

        var content = document.getElementById('content-dialog');

        content.innerHTML =
            '        <div id="siteNotice ">\n' +
            '            <div class="card row no-gutters shadow bg-white rounded-lg overflow-hidden" style=" max-height: 240px;">\n' +
            '               <div class="row no-gutters">\n' +
            '                   <div class="col-md-4">\n' +
            '                <img src="' + image + '" class="card-img" alt="">\n' +
            '                   </div>\n' +
            '               <div class="col-md-8 ">\n' +
            '                <div class="card-body" style="height: 250px;overflow-y:scroll;">\n' +
            '                    <h5 class="card-title">\n' +
            '                        <div id="firstHeading" class="firstHeading">' + title + '</div>\n' +
            '                    </h5>\n' +
            '                    <div id="bodyContent" >\n' +
            '                        <p class="card-text" id="bloque" >\n' +
            '                        <p class="overflow-hidden"><b>' + title + '</b>, ' + description + '\n' +
            '                <p class="card-text " id="bloque"><p><b>' + calle + ' ' + nro + ', ' + provincia + '</b><br> \n' +
            '                    </div>\n' +
            '                </div>\n' +
            '              </div>\n' +
            '               </div>\n' +
            '             </div>\n' +
            '          </div>';


        modal.style.display = "block";


    }
</script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase.js"></script>
<script>
    // Initialize Firebase
    console.log('Initialize Firebase');
    var config = {
        apiKey: "AIzaSyBwfELRbz6TWmqEQAjKWI4u7aQ9V8FL8Nw",
        authDomain: "comer-bien.firebaseapp.com",
        databaseURL: "https://comer-bien.firebaseio.com",
        projectId: "comer-bien",
        storageBucket: "comer-bien.appspot.com",
        messagingSenderId: "471498877486"
    }
    firebase.initializeApp(config);
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt4-eHHq80AgKWr9fOgM9m7eZbyqZpYDI&callback=initMap">
</script>
<script>
    var database = firebase.database();
    var map;
    var markers = [];
    var institucionesList = [];
    var comboProvincias = document.getElementById("provinciasList");
    var comboMunicipios = document.getElementById("municipiosList");
    // var comboEnergias = document.getElementById("energiasList");
    var comboServicios = document.getElementById("serviciosList");
    var buttonRefrescar = document.getElementById("buttonRefrescar");
    var atributeTypeEnergia = "TipoDeEnergia";//"energia";

    function initMap() {
        console.log('initMap');
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(-34.603722, -58.381592),
            zoom: 14

        });
    }


    function getUnique(arr,DatosDeLaInstitucion,NombreInstitucion) {

        const unique = arr
            .map(e => e[DatosDeLaInstitucion][NombreInstitucion])

            // store the keys of the unique objects
            .map((e, i, final) => final.indexOf(e) === i && i)

            // eliminate the dead keys & store unique objects
            .filter(e => arr[e]).map(e => arr[e]);

        return unique;
    }

    var agregarTarjetas = function agregarTarjetas(init, last) {
        console.log('agregarTarjetas');
        var cardsContainer = document.getElementById("cardsContainer");
        cardsContainer.innerHTML = "";
        let institucionesDistinct =  getUnique(institucionesList,'DatosDeLaInstitucion','nombreinstitucion');

        //Paginación
        let num = institucionesDistinct.length;
        let res = num /8;
        let numPaginas = Math.ceil(res);


        for (i = 0; i < institucionesDistinct.length; i++) {
            if (i>= init && i<last)
            {
                var title = institucionesDistinct[i].DatosDeLaInstitucion.nombreinstitucion;
                var description = institucionesDistinct[i].DatosDeLaInstitucion.descripcioninstitucion;
                var image = institucionesDistinct[i].Imagen;
                var calle = institucionesDistinct[i].DatosDeLaInstitucion.calle;
                var nro = institucionesDistinct[i].DatosDeLaInstitucion.numero;
                var provincia = institucionesDistinct[i].DatosDeLaInstitucion.provincia;

                image = institucionesDistinct[i].Imagen;
                if (image == "")
                {
                    image = "./img/default.jpg"
                }

                //if (institucionesDistinct[i].add == false) {
                    cardsContainer.innerHTML = cardsContainer.innerHTML +
                        '         <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-around"  id="content">\n' +
                        '            <div class="card mt-5 shadow mb-5 bg-white rounded-lg " style="width: 12rem;" id="siteNotice">\n' +
                        '                <img src=' + image + ' class="card-img-top" alt="">\n' +
                        '            <div class="card-body">\n' +
                        '                <h5 class="card-title"><div id="firstHeading" class="firstHeading">' + institucionesDistinct[i].DatosDeLaInstitucion.nombreinstitucion + '</div></h5>\n' +
                        '            <div id="bodyContent">\n' +
                        '                <p class="card-text " id="bloque"><p><b>' + institucionesDistinct[i].DatosDeLaInstitucion.nombreinstitucion + '</b>' + institucionesDistinct[i].DatosDeLaInstitucion.descripcioninstitucion.substring(0, 50) + '<br> \n' +
                        "<a href='javascript:openModal(\"" + title + "\",\"" + description + "\",\"" + image + "\",\"" + calle + "\",\"" + nro + "\",\"" + provincia + "\");' class='btn btn-outline-none btn-sm mt-2' id='boton'>Mas Info</a>" + '\n' +
                        '            </div>\n' +
                        '            </div>\n' +
                        '            </div>\n' +
                        '        </div> \n'
                    ;

                    //institucionesDistinct[i].add = true;
                //}
            }
        }
        if (numPaginas > 1)
        {
            var prev = 0;
            var prevLast = 0;
            if (init != 0) prev = init - 8;
            var next = 8;
            var nextLast = 8;
            if (next != 8) next = last + 8;
            prevLast = prev + 8;
            nextLast = next + 8;

            var inicio = 0;
            var fin = 8;

            cardsContainer.innerHTML = cardsContainer.innerHTML + '<div class="col-12 "/>\n';
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '   <nav>\n' +
                '       <ul class="pagination">\n' +
                '           <li class="page-item"><a  href="javascript:agregarTarjetas(' + prev + ',' + prevLast +');" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span></a></li>\n';
            for (i=1; i <= numPaginas; i++)
            {
                cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetas(' + inicio + ',' + fin +');" class="page-link">' + i + '</a></li>\n';
                inicio= inicio + 8;
                fin = fin + 8;
            }
            cardsContainer.innerHTML = cardsContainer.innerHTML +
                '           <li class="page-item"><a href="javascript:agregarTarjetas(' + next + ',' + nextLast +');" class="page-link" aria-label="Next"><span aria-hidden="true">»</span></a></li>\n' +
                '       </ul>\n' +
                '    </nav>\n';
        }
    };


    function addMarker(lat, lng, title, description, image, calle, nro, provincia) {
        console.log("addMarker: ");
        console.log("lat: " + lat);
        console.log("lng: " + lng);
        console.log("title: " + title);
        console.log("description: " + description);

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            title: title,

        });

        if (image == "")
        {
            image = "./img/default.jpg"
        }

        var contentString =
            '        <div id="siteNotice ">\n' +
            '            <div class="card row no-gutters shadow bg-white rounded-lg overflow-hidden" style=" max-height: 240px;">\n' +
            '               <div class="row no-gutters">\n' +
            '                   <div class="col-md-4">\n' +
            '                <img src="' + image + '" class="card-img" alt="">\n' +
            '                   </div>\n' +
            '               <div class="col-md-8 ">\n' +
            '                <div class="card-body">\n' +
            '                    <h5 class="card-title">\n' +
            '                        <div id="firstHeading" class="firstHeading">' + title + '</div>\n' +
            '                    </h5>\n' +
            '                    <div id="bodyContent">\n' +
            '                        <p class="card-text" id="bloque" >\n' +
            '                        <p class="overflow-hidden"><b>' + title + '</b>, ' + description.substring(0, 50) + '\n' +
            '                    </div>\n' +
            '                </div>\n' +
            "<a href='javascript:openModal(\"" + title + "\",\"" + description + "\",\"" + image + "\",\"" + calle + "\",\"" + nro + "\",\"" + provincia + "\");' class='btn btn-outline-none btn-sm mt-2' id='boton'>Mas Info</a>" + '\n' +
            '              </div>\n' +
            '               </div>\n' +
            '             </div>\n' +
            '          </div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString,

        });


        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        markers.push(marker);
    }

    function deleteAllMarkers() {
        //Loop through all the markers and remove
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    };

    function moveCamera(lat, lng, zoom) {
        console.log("move camera ");
        console.log("lat: " + lat);
        console.log("lng: " + lng);
        if (lat == null && lng == null) {
            console.log("move camera returning...");
            return;
        }
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: {lat: lat, lng: lng},
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            fullscreenControl: true
        });

    }

    function selectElement(id, valueToSelect) {
        var element = document.getElementById(id);
        element.value = valueToSelect;
    }


</script>
<script>

    function loadTodos(provinciaId, partidoId, servicios){
        console.log("loadTodos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId;
        console.log("ref: " + reference);

        if (servicios === "merenderoy comedor") {

            database
                .ref(reference)
                .once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function (childSnapshot) {
                        //console.log(childSnapshot.key);
                        //console.log(childSnapshot.val());
                        childSnapshot.forEach(function (childSnapshots) {
                            addMarker(
                                childSnapshots.val().latitude,
                                childSnapshots.val().longitude,
                                childSnapshots.val().DatosDeLaInstitucion.nombreinstitucion,
                                childSnapshots.val().DatosDeLaInstitucion.descripcioninstitucion,
                                childSnapshots.val().Imagen,
                                childSnapshots.val().DatosDeLaInstitucion.calle,
                                childSnapshots.val().DatosDeLaInstitucion.numero,
                                childSnapshots.val().DatosDeLaInstitucion.provincia
                            );

                            var obj = childSnapshots.val();
                            obj.add = false;
                            institucionesList.push(obj);
                        })
                    });
                })
                .then(function (tarjetas) {
                    agregarTarjetas(0, 8);
                });
        }
    }


    function loadMerenderos(provinciaId, partidoId) {
        console.log("loadMerenderos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId + "/merendero";
        console.log("ref: " + reference);

        database
            .ref(reference)
            .once('value')
            .then(function (snapshot) {
                //console.log(snapshot.key);
                snapshot.forEach(function (childSnapshot) {
                    //console.log(childSnapshot.key);
                    //console.log(childSnapshot.val());


                    addMarker(
                        childSnapshot.val().latitude,
                        childSnapshot.val().longitude,
                        childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion,
                        childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion,
                        childSnapshot.val().Imagen,
                        childSnapshot.val().DatosDeLaInstitucion.calle,
                        childSnapshot.val().DatosDeLaInstitucion.numero,
                        childSnapshot.val().DatosDeLaInstitucion.provincia
                    );

                    var obj = childSnapshot.val();
                    obj.add = false;
                    institucionesList.push(obj);
                });
            })
            .then(function (tarjetas) {
                agregarTarjetas(0, 8);
            });

    }

    function loadComederos(provinciaId, partidoId) {
        console.log("loadComederos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId + "/comederos";
        console.log("ref: " + reference);


        database
            .ref(reference)
            .once('value')
            .then(function (snapshot) {
                //console.log(snapshot.key);
                snapshot.forEach(function (childSnapshot) {
                    //console.log(childSnapshot.key);
                    console.log(childSnapshot.val());

                    addMarker(
                        childSnapshot.val().latitude,
                        childSnapshot.val().longitude,
                        childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion,
                        childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion,
                        childSnapshot.val().Imagen,
                        childSnapshot.val().DatosDeLaInstitucion.calle,
                        childSnapshot.val().DatosDeLaInstitucion.numero,
                        childSnapshot.val().DatosDeLaInstitucion.provincia
                    );

                    var obj = childSnapshot.val();
                    obj.add = false;
                    institucionesList.push(obj);

                });
            })
            .then(function (tarjetas) {
                agregarTarjetas(0, 8);
            });

    }

    var clearSelect = function removeOptions(documentId) {
        selectbox = document.getElementById(documentId);
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    switch (document.getElementById("serviciosList").value)
    {
        case "Merendero y Comedor":
            loadTodos();
            break;
        case "Merenderos":
            loadMerenderos();
            break;
        case "Comedores":
            loadComederos();
            break;
    }

    function cargarMarcas(provinciaId, municipioId) {

        database
            .ref("provincias/" + provinciaId + "/partidos/" + municipioId)
            .once('value')
            .then(function (snapshot) {
                moveCamera(snapshot.val().latitude, snapshot.val().longitude, snapshot.val().zoom);

                var valServ = comboServicios.options[comboServicios.selectedIndex].text.toLowerCase().trim().replace(' ', '');

                console.log("valServ: " + valServ);
                // console.log("valEnerg: " + valEnerg);

                if (valServ === "merenderoy comedor") {
                    console.log("camino 1")
                    loadTodos(provinciaId, municipioId, valServ);

                } else if (valServ === "merenderos") {
                    console.log("camino 2");
                    loadMerenderos(provinciaId, municipioId);

                    console.log("camino 3");
                } else if (valServ === "comedores") {
                    loadComederos(provinciaId, municipioId);

                }
            });

    }

    var loadMunis = function loadMunicipios() {

        clearSelect("municipiosList");
        database
            .ref("provincias/" + comboProvincias.options[comboProvincias.selectedIndex].value + "/partidos")
            .once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childKey = childSnapshot.key;
                    //var childName = childSnapshot.val().nombre;
                    comboMunicipios.innerHTML = comboMunicipios.innerHTML + "<option selected value='" + childKey + "'>" + childKey + "</option>"
                    selectElement('municipiosList', 0);
                });
            });

        comboMunicipios.onchange = function () {
            deleteAllMarkers();
            // cargarMarcas(comboProvincias.options[comboProvincias.selectedIndex].value, comboMunicipios.options[comboMunicipios.selectedIndex].value);
            var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias.options[comboProvincias.selectedIndex].value + ',+' + comboMunicipios.options[comboMunicipios.selectedIndex].value + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
            console.log(url)
            fetch(url)
                .then(function (response) {
                    return response.json();

                })
                .then(function (json) {
                    //console.log(json);
                    moveCamera(json.results[0].geometry.location.lat, json.results[0].geometry.location.lng, 14);
                });
        }

    }
    var loadProv = function loadProvincias() {


        clearSelect("provinciasList");
        clearSelect("municipiosList");
        database
            .ref("provincias")
            .once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childKey = childSnapshot.key;
                    //var childName = childSnapshot.val().nombre;
                    comboProvincias.innerHTML = comboProvincias.innerHTML + "<option selected value='" + childKey + "'>" + childKey + "</option>"
                    selectElement('provinciasList', 0);
                });
            });
        comboProvincias.onchange = function () {

            loadMunis();

            database
                .ref("provincias/" + comboProvincias.options[comboProvincias.selectedIndex].value)
                .once('value')
                .then(function (snapshot) {
                    if (snapshot.val().latitude != null && snapshot.val().longitude != null) {
                        moveCamera(snapshot.val().latitude, snapshot.val().longitude, snapshot.val().zoom);
                    }
                });
        }
    }

    loadProv();
    buttonRefrescar.onclick = function () {
        console.log("refrescando...");

        var cardsContainer = document.getElementById("cardsContainer");
        cardsContainer.innerHTML = "";

        institucionesList = [];


        deleteAllMarkers();
        cargarMarcas(comboProvincias.options[comboProvincias.selectedIndex].value, comboMunicipios.options[comboMunicipios.selectedIndex].value);
        document.getElementById('cardsContainer').innerHTML = "";


        var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + 'Argentina' + ',+' + comboProvincias.options[comboProvincias.selectedIndex].value + ',+' + comboMunicipios.options[comboMunicipios.selectedIndex].value + '&key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0';
        console.log(url)
        fetch(url)
            .then(function (response) {
                return response.json();

            })
            .then(function (json) {
                //console.log(json);
                moveCamera(json.results[0].geometry.location.lat, json.results[0].geometry.location.lng, 14);
            });
    }


</script>


</body>
</html>





















