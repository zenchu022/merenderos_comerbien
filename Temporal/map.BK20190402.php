<!DOCTYPE html >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">


    <title>Mapa de Comederos y Merenderos</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 450px;
        }


        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        h5 {
            font-size: 15px;
            border-bottom: 2px solid #f2c94c;
            font-weight: 600;
            color: #949494;

        }

        #bloque {
            margin-top: -15px;
            font-size: 11px;
            position: absolute;
            float: left;
        }

        #boton {
            text-decoration: underline;
            color: #007fee;
            position: relative;
            float: right;
        }

        #boton:hover {
            color: #79c0ff;

        }
    </style>
</head>

<html>
<body>

<div class="container">

    <div class="row shadow shadow p-3 mb-5 bg-white rounded mt-2 justify-content-arround">
        <div id="filterCitys" class="formulario">
            <form>
                <fieldset class="row">
                    <div class="col-9 col-md-12">
                        <h1>
                            <legend>Filtros</legend>
                        </h1>
                        <p>
                            <label>Provincia</label>
                            <select id="provinciasList">
                            </select>
                            <label>Partidos</label>
                            <select id="municipiosList">
                            </select>
                            <label>Energia</label>
                            <select id="energiasList">

                                <option selected value="todos">Todos</option>
                                <option selected value="natural">Gas natural</option>
                                <option selected value="garrafa">Garrafa</option>
                                <option selected value="lenia">Leña</option>
                                <option selected value="otro">Otro</option>
                            </select>
                            <label>Servicios</label>
                            <select id="serviciosList">
                                <option selected value=0>Todos</option>
                                <option selected value=1>Merenderos</option>
                                <option selected value=2>Comedores</option>
                            </select>
                            <a id='buttonRefrescar' class="btn btn-primary">Refrescar</a>
                        </p>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <div class="row img-fluid"></div>

    <div id="map" class="col map-container img-fluid shadow p-3 mb-5 rounded"></div>

    <div id="cardsContainer" class="row"/>
<!--    //class="col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-around"/>-->

</div>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase.js"></script>
<script>
    // Initialize Firebase
    console.log('Initialize Firebase');
    var config = {
        apiKey: "AIzaSyBwfELRbz6TWmqEQAjKWI4u7aQ9V8FL8Nw",
        authDomain: "comer-bien.firebaseapp.com",
        databaseURL: "https://comer-bien.firebaseio.com",
        projectId: "comer-bien",
        storageBucket: "comer-bien.appspot.com",
        messagingSenderId: "471498877486"
    }
    firebase.initializeApp(config);
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt4-eHHq80AgKWr9fOgM9m7eZbyqZpYDI&callback=initMap">
</script>
<script>
    var database = firebase.database();
    var map;
    var markers = [];
    var institucionesList = [];
    var comboProvincias = document.getElementById("provinciasList");
    var comboMunicipios = document.getElementById("municipiosList");
    var comboEnergias = document.getElementById("energiasList");
    var comboServicios = document.getElementById("serviciosList");
    var buttonRefrescar = document.getElementById("buttonRefrescar");
    var atributeTypeEnergia = "TipoDeEnergia";//"energia";

    function initMap() {
        console.log('initMap');
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(-34.603722, -58.381592),
            zoom: 14

        });
    }

    var agregarTarjetas = function agregarTarjetas() {
        console.log('agregarTarjetas');
        var cardsContainer = document.getElementById("cardsContainer");

        // var row1 = document.createElement('div');
        // row1.class = "col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-around"

        for (i = 0; i < institucionesList.length; i++) {
            if (institucionesList[i].add == false) {
                //var col1 = document.createElement('div');
                //cal1.class = "col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-around";
                cardsContainer.innerHTML = cardsContainer.innerHTML +
                    //  '<div class="row ">\n' +
                    '         <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-around"  id="content">\n' +
                    '            <div class="card mt-5 shadow mb-5 bg-white rounded-lg " style="width: 12rem;" id="siteNotice">\n' +
                    '                <img src=' + institucionesList[i].Imagen + ' class="card-img-top" alt="">\n' +
                    '            <div class="card-body">\n' +
                    '                <h5 class="card-title"><div id="firstHeading" class="firstHeading">' + institucionesList[i].DatosDeLaInstitucion.nombreinstitucion + '</div></h5>\n' +
                    '            <div id="bodyContent">\n' +
                    '                <p class="card-text " id="bloque"><p><b>' + institucionesList[i].DatosDeLaInstitucion.nombreinstitucion + '</b>' + institucionesList[i].DatosDeLaInstitucion.descripcioninstitucion.substring(0, 50) + '<br> \n' +
                    '                <a href="#" class="btn btn-outline-none btn-sm mt-2" id="boton">Mas Info</a>\n' +
                    '            </div>\n' +
                    '            </div>\n' +
                    '            </div>\n' +
                    '        </div> \n'// +
                //'    </div>'
                ;

                //row1.appendChild(col1);
                //cardsContainer.appendChild(col1);
                institucionesList[i].add = true;
            }
        }
        //cardsContainer.appendChild(row1);
    }


    function addMarker(lat, lng, title, description, image) {
        console.log("addMarker: ");
        console.log("lat: " + lat);
        console.log("lng: " + lng);
        console.log("title: " + title);
        console.log("description: " + description);

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            title: title,

        });

        var contentString =
            '        <div id="siteNotice ">\n' +
            '            <div class="card row no-gutters shadow bg-white rounded-lg overflow-hidden" style=" max-height: 240px;">\n' +
            '               <div class="row no-gutters">\n' +
            '                   <div class="col-md-4">\n' +
            '                <img src="' + image + '" class="card-img" alt="">\n' +
            '                   </div>\n' +
            '               <div class="col-md-8 ">\n' +
            '                <div class="card-body">\n' +
            '                    <h5 class="card-title">\n' +
            '                        <div id="firstHeading" class="firstHeading">' + title + '</div>\n' +
            '                    </h5>\n' +
            '                    <div id="bodyContent">\n' +
            '                        <p class="card-text" id="bloque" >\n' +
            '                        <p class="overflow-hidden"><b>' + title + '</b>, ' + description + '\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '                        <a href="#" class="btn btn-outline-none btn-sm text-wrap mr-2" id="boton">Mas Info</a>\n' +
            '              </div>\n' +
            '               </div>\n' +
            '             </div>\n' +
            '          </div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString,

        });


        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        markers.push(marker);
    }

    function deleteAllMarkers() {
        //Loop through all the markers and remove
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    };

    function moveCamera(lat, lng, zoom) {
        console.log("move camera ");
        console.log("lat: " + lat);
        console.log("lng: " + lng);
        if (lat == null && lng == null) {
            console.log("move camera returning...");
            return;
        }
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: {lat: lat, lng: lng},
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            fullscreenControl: true
        });

    }

    function selectElement(id, valueToSelect) {
        var element = document.getElementById(id);
        element.value = valueToSelect;
    }


</script>
<script>
    var clearSelect = function removeOptions(documentId) {
        selectbox = document.getElementById(documentId);
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    var loadMerenderos = function loadMerenderos(provinciaId, partidoId, energia) {
        console.log("loadMerenderos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId + "/merenderos";
        console.log("ref: " + reference);

        console.log('energia val : ' + energia);

        if (energia === "todos") {
            database
                .ref(reference)
                .once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function (childSnapshot) {
                        //console.log(childSnapshot.key);
                        //console.log(childSnapshot.val());

                        addMarker(childSnapshot.val().latitude, childSnapshot.val().longitude, childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion, childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion, childSnapshot.val().Imagen);
                        var obj = childSnapshot.val();
                        obj.add = false;
                        institucionesList.push(obj);
                        agregarTarjetas();
                    });
                });
        } else {
            database
                .ref(reference)
                .orderByChild(atributeTypeEnergia).equalTo(energia)
                .once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function (childSnapshot) {
                        //console.log(childSnapshot.key);
                        //console.log(childSnapshot.val());

                        addMarker(childSnapshot.val().latitude, childSnapshot.val().longitude, childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion, childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion, childSnapshot.val().Imagen);
                        var obj = childSnapshot.val();
                        obj.add = false;
                        institucionesList.push(obj);
                        agregarTarjetas();
                    });
                });
        }
    }

    var loadComederos = function loadComederos(provinciaId, partidoId, energia) {
        console.log("loadComederos");
        var reference = "provincias/" + provinciaId + "/partidos/" + partidoId + "/comederos";
        console.log("ref: " + reference);

        if (energia === "todos") {
            database
                .ref(reference)
                .once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function (childSnapshot) {
                        //console.log(childSnapshot.key);
                        console.log(childSnapshot.val());

                        addMarker(childSnapshot.val().latitude, childSnapshot.val().longitude, childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion, childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion, childSnapshot.val().Imagen);
                        var obj = childSnapshot.val();
                        obj.add = false;
                        institucionesList.push(obj);
                        agregarTarjetas();

                    });
                });
        } else {
            database
                .ref(reference)
                .orderByChild(atributeTypeEnergia).equalTo(energia)
                .once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function (childSnapshot) {
                        //console.log(childSnapshot.key);
                        //console.log(childSnapshot.val());

                        addMarker(childSnapshot.val().latitude, childSnapshot.val().longitude, childSnapshot.val().DatosDeLaInstitucion.nombreinstitucion, childSnapshot.val().DatosDeLaInstitucion.descripcioninstitucion, childSnapshot.val().Imagen);
                        var obj = childSnapshot.val();
                        obj.add = false;
                        institucionesList.push(obj);
                        agregarTarjetas();
                    });
                });
        }
    }

    function cargarMarcas(provinciaId, municipioId) {

        database
            .ref("provincias/" + provinciaId + "/partidos/" + municipioId)
            .once('value')
            .then(function (snapshot) {
                moveCamera(snapshot.val().latitude, snapshot.val().longitude, snapshot.val().zoom);

                var valServ = comboServicios.options[comboServicios.selectedIndex].text.toLowerCase().trim().replace(' ', '');
                var valEnerg = comboEnergias.options[comboEnergias.selectedIndex].value;

                console.log("valServ: " + valServ);
                console.log("valEnerg: " + valEnerg);

                if (valServ === "todos") {
                    console.log("camino 1");
                    loadMerenderos(provinciaId, municipioId, valEnerg);
                    loadComederos(provinciaId, municipioId, valEnerg);

                } else if (valServ === "merenderos") {
                    console.log("camino 2");
                    loadMerenderos(provinciaId, municipioId, valEnerg);

                    console.log("camino 3");
                } else if (valServ === "comedores") {
                    loadComederos(provinciaId, municipioId, valEnerg);

                }
            });

    }

    var loadMunis = function loadMunicipios() {

        clearSelect("municipiosList");
        database
            .ref("provincias/" + comboProvincias.options[comboProvincias.selectedIndex].value + "/partidos")
            .once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childKey = childSnapshot.key;
                    //var childName = childSnapshot.val().nombre;
                    comboMunicipios.innerHTML = comboMunicipios.innerHTML + "<option selected value='" + childKey + "'>" + childKey + "</option>"
                    selectElement('municipiosList', 0);
                });
            });

        comboMunicipios.onchange = function () {
            deleteAllMarkers();
            cargarMarcas(comboProvincias.options[comboProvincias.selectedIndex].value, comboMunicipios.options[comboMunicipios.selectedIndex].value);
        }

    }
    var loadProv = function loadProvincias() {


        clearSelect("provinciasList");
        clearSelect("municipiosList");
        database
            .ref("provincias")
            .once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childKey = childSnapshot.key;
                    //var childName = childSnapshot.val().nombre;
                    comboProvincias.innerHTML = comboProvincias.innerHTML + "<option selected value='" + childKey + "'>" + childKey + "</option>"
                    selectElement('provinciasList', 0);
                });
            });
        comboProvincias.onchange = function () {

            loadMunis();

            database
                .ref("provincias/" + comboProvincias.options[comboProvincias.selectedIndex].value)
                .once('value')
                .then(function (snapshot) {
                    if (snapshot.val().latitude != null && snapshot.val().longitude != null) {
                        moveCamera(snapshot.val().latitude, snapshot.val().longitude, snapshot.val().zoom);
                    }
                });
        }
    }

    loadProv();
    buttonRefrescar.onclick = function () {
        console.log("refrescando...");
        deleteAllMarkers();
        cargarMarcas(comboProvincias.options[comboProvincias.selectedIndex].value, comboMunicipios.options[comboMunicipios.selectedIndex].value);
        document.getElementById('cardsContainer').innerHTML = "";
    }


</script>


</body>
</html>





















