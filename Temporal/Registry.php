<!DOCTYPE html >

<head xmlns="http://www.w3.org/1999/html">



    <meta charset="utf-8">
    <link rel="stylesheet" href="eldiseño.css">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"

     integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

     <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">



</head>

<html>

<body>




<script>

    window.onload = function() {

        document.getElementById('submitFirebase').onclick = function() {

            //document.getElementById('setFirebaseData').submit();

            return false;

        };

    };

</script>



<div class="container">



    <div class="row ml-5 mr-5 mt-5 justify-content-arroun" >
        <h2>Formulario para la georeferenciacion de merenderos y comedores</h2>
    </div>

    <form id="setFirebaseData" action="" method="POST">

    <div class="form-group row  justify-content-arround ml-5 mr-5  p-3 rounded" id="fondoa">
        <div class="col-12 "> 
            <h5>Datos de la institución</h5>
        </div>
        <div class="col-12 col-md-6">
            <label for="nombreinstitucion"></label>
            <input type="text" name="nombreinstitucion" id="nombreinstitucion" class="form-control"  
            placeholder="Nombre de la Institucion">
        </div>
        <div class="col-12 col-md-6">
        <label for="servicioqueofrece"></label>
        <select id="servicioqueofrece"  class="form-control">
            <option disabled selected>Servicio Que Ofrece</option>
            <option value="merendero">Merendero</option>
            <option value="Comedor">Comedor</option>
            <option value="merenderocomedor">Merendero y Comedor</option>
        </select>
        </div>
        <div class="col-12 mt-3">
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Descripción de la Institución"></textarea>
        </div>
        <div class="col-12 mt-3">
            <h6 for="vinculo">Esta vinculado con alguna organización / institución</h6>
        </div>
        <div class="col-3">
        <select id="vinculo"  class="form-control">
            <option value="si">Si</option>
            <option value="no">No</option>
        </select>
        </div>
        

    <div class="col-9">
    <input type="text" name="explicacion" id="explicacion" class="form-control" placeholder="Nombre de la organización/institución">
    </div>
</div>

    <div class="row  justify-content-arround ml-5 mr-5  p-3 rounded" id="fondob">
        <div class="col-12"><h5>Datos de localización</h5> </div>
        <div class="col-6 col-md-3">
        <label for="provincias"></label>
        <select id="selectProvincia" class="form-control">
            <option disabled selected>Provincias</option>
            <option value="CBA">Cordoba</option>
            <option value="CABA">Buenos Aires</option>
            <option value="SF">Santa Fe</option>
        </select>
        </div>
    
    <div class="col-6 col-md-3">
        <label for="distritos"></label>
        <select id="selectDistritos" class="form-control">
            <option disabled selected>Distritos</option>
            <option value="PAL">Palermo</option>
            <option value="MON">Moreno</option>
            <option value="NUN">Nuñez</option>
        </select>
    </div>

    <div class="col-12 col-md-6">
        <label for="calle"></label>
        <input type="text" name="calle" id="calle" class="form-control" placeholder="Calle">
    </div>
    <div class="col-12 col-md-5">
        <label for="barrio"></label>
        <input type="text" name="barrio" id="barrio" class="form-control"placeholder="Barrio">
    </div>

    <div class="col-12 col-md-2">
        <label for="numero"></label>
        <input type="text" name="numero" id="numero" class="form-control" placeholder="Numero">

    </div>
    <div class="col-12 col-md-5">
        <label for="entrecalle"></label>
        <input type="text" name="entrecalle" id="entrecalle" class="form-control" placeholder="Entre calle y/o manzana">
    </div>

    

 </div>

<div class="row mt-4 justify-content-arround ml-5 mr-5   p-3 rounded" id="fondoc">
    <div class="col-12"><h5>Otros datos importantes</h5></div>
        <div class="col-12"><h6>Dias de funcionamiento y horario</h6></div>
        <div class="col-6 col-md-6">
            <select id="Dia" class="form-control" >
                <option disabled selected>Dia</option>
                <option value="Lunes">Lunes</option>
                <option value="Martes">Martes</option>
                <option value="Miercoles">Miercoles</option>
                <option value="Jueves">Jueves</option>
                <option value="Viernes">Viernes</option>
                <option value="Sabado">Sabado</option>
                <option value="Domingo">Domingo</option>
                <option value="LunesMiercolesViernes">Lunes - Miercoles - Viernes</option>
                <option value="MartesJueves">Martes - Jueves</option>
            </select>
        </div>

    <div class="col-6 col-md-6">
        <input type="text" name="hora" id="hora"  class="form-control" placeholder="Hora">
    </div>
        <div class="col-12 mt-3">
        <label for="otrasactividades"><h6>Aparte de funcionar como merendero / comedor ¿Se desarrollan otras actividades en el lugar?</h6></label>
        </div>
        <div class="col-12 col-md-12">
        <select id="otrasactividades"  class="form-control">
            <option value="si">Sí</option>
            <option value="no">No</option>
        </select>
    </div>  

    <div class="col-6 col-md-7 ml-4 mt-3"> 
        <label for="respuestasi"><h6>Si respondió "Sí", por favor indique cuales (puede elegir más de una)</h6> </label>
        <br/>
        <input type="checkbox" class="form-check-input" name="apoyo" id="apoyo" value="apoyo escolar">Apoyo Escolar</input>
        <br/>
        <input type="checkbox" class="form-check-input" name="TerminalidadEducativa" id="TerminalidadEducativa" value="Programa de terminalidad educativa">Programa de terminalidad educativa</input>
        <br/>
        <input type="checkbox" class="form-check-input" name="ActividadesCulturales" id="ActividadesCulturales" value="Actividades Culturales">Actividades Culturales</input>
        <br/>
        <input type="checkbox" class="form-check-input" name="TalleresOficios" id="TalleresOficios" value="Talleres de oficios">Talleres de oficios</input>
        <br/>
        <input type="checkbox" class="form-check-input" name="ActividadesDeportivas" id="ActividadesDeportivas" value="Actividades deportivas">Actividades deportivas</input>
        <br/>
        <input type="checkbox" class="form-check-input" name="otras" value="Otras">Otras</input>
        <label for="otras"></label>
        <input type="text" name="cuales" id="cuales" class="form-control" placeholder="Cuales" >
    </div>

    <div class="col-12 col-md-12  input-group mb-3 mt-3">
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="inputGroupFile02" >
            <label class="custom-file-label" for="inputGroupFile02" >Seleccione archivo</label>
        </div>
        <div class="input-group-append">
            <span class="input-group-text" id="">Subir</span>        
        </div>
    </div>

    </div>
    <div class="row row mt-4 justify-content-arround ml-5 mr-5   p-3 rounded" id="fondod">
        <div class="col-12"><h5>Datos internos de la red</h5></div>
         <div class="col-12 mt-3"><h6>Menciona el mes en comenzó a funcionar el merendero/ comedor</h6></div>
    <div class="col-6">
    <label for="anio"></label>
    <select id="anio"  class="form-control">
        <option disabled selected>Año</option>
        <option value="1990">1990</option>
        <option value="2030">2030</option>
    </select>

</div>
<div class="col-6">
    <label for="mes"></label>
    <select id = "mes" class="form-control" >
    <option disabled selected>Mes</option>
        <option value="enero">Enero</option>
        <option value="febrero">Febrero</option>
        <option value="marzo">Marzo</option>
        <option value="abril">Abril</option>
        <option value="mayo">Mayo</option>
        <option value="junio">Junio</option>
        <option value="julio">Julio</option>
        <option value="agosto">Agosto</option>
        <option value="septiembre">Septiembre</option>
        <option value="octubre">Octubre</option>
        <option value="noviembre">Noviembre</option>
        <option value="diciembre">Diciembre</option>
    </select>
</div>
    
<div class="col-12 col-md-6 mt-3">
<h6 for="comensales">Cuantas personas asisten en el merendero / comedor</h6>
<select id="comensales"  class="form-control">
    <option value="1-10">1 y 10</option>
    <option value="11-20">11 y 20</option>
    <option value="20+">Mas de 20</option>
</select>
</div>

<div class="col-12 col-md-6 mt-3">
<h6 for="asistentes">Cuantas personas trabajan en el merendero / comedor</h6>
<select id="asistentes"  class="form-control">
    <option value="1-50">1 y 50</option>
    <option value="51-100">51 y 100</option>
    <option value="101-50">101 y 50</option>
    <option value="151-200">151 y 200</option>
    <option value="200+">Mas de 200</option>
</select>

</div>
    <div class="col-12 mt-3">
        <h6 for="tipoenergia">Que tipo de energia se utiliza en el merendero / comedor</h6>
        <select id="tipoenergia"  class="form-control">
            <option value="natural">Gas Natural</option>
            <option value="garrafa">Gas envasado (garrafa / tubo / tanque)</option>
            <option value="lenia">Leña</option>
            <option value="otro">Otro</option>
        </select>

    </div>   

    </div>
    


<div class="row justify-content-arround ml-5 mr-5 mt-4 p-3 rounded" id="fondoe">


    <div class="col-12">

    <label for="lbldatosresponsable"><h5>Datos de contacto con responsable del merendero / comedor</h5></label>

    </div>



    <div class="col-6">

    <label for="nombreresponsable"></label>

    <input type="text" name="nombreresponsable" id="nombreresponsable"  class="form-control" placeholder="Nombre">

    </div>



    <div class="col-6">

    <label for="telefonoresponsable"></label>

    <input type="text" name="telefonoresponsable" id="telefonoresponsable"  class="form-control" placeholder="Telefeono">



    </div>



    <div class="col-12">

    <label for="mailresponsable"></label>

    <input type="text" name="temailresponsable" id="mailresponsable"  class="form-control" placeholder="Correo Electronico">

    </div>

    
</div>

    <div class="row  mt-5 mb-5 justify-content-center">
    <div class="col-4 text-center  align-self-center">
    <a href="#" id="submitFirebase" class="button">submit</a>
    </div>
</div>
</form>
</div>
</body>
</html>

