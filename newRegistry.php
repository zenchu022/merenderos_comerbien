<!DOCTYPE html >
<html>
<head xmlns="http://www.w3.org/1999/html">
    <meta charset="utf-8">
    <link rel="stylesheet" href="eldiseño.css">
    <link rel="stylesheet" href="validin.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
     integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">
</head>

<body>
<!-- Iniciación Firebase -->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0&libraries=places&callback=initAutocomplete" async defer></script>-->
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!--<script src="https://code.jquery.com/ui/3.3.1/jquery-ui.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="waitingfor.js"></script>
<script src="validin.js"></script>
<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0"></script>-->
<script>
    // Initialize Firebase
    console.log('Initialize Firebase');

    var key_selected_institute = "";

    var config = {
        apiKey: "AIzaSyBwfELRbz6TWmqEQAjKWI4u7aQ9V8FL8Nw",
        authDomain: "comer-bien.firebaseapp.com",
        databaseURL: "https://comer-bien.firebaseio.com",
        projectId: "comer-bien",
        storageBucket: "comer-bien.appspot.com",
        messagingSenderId: "471498877486"
    }

    var geocoder = null;
    var adress = null;


    firebase.initializeApp(config);

    var placeSearch, autocomplete;
    var geolocation;
    var database = firebase.database();

    var comboMunicipios;// = document.getElementById("selectDistritos");
    var comboProvincias;// = document.getElementById("selectProvincia");

    var lat;
    var lng;

    var update = false;

    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        sublocality_level_1: 'long_name',
        neighborhood: 'long_name',
        administrative_area_level_1: 'long_name',
        administrative_area_level_2: 'long_name',
        country: 'long_name',
        postal_code: 'short_name',
        postal_code_suffix: 'short_name'
    };

    const array_establecimientos = [];

    function initAutocomplete() {

        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('calle'), {types: ['geocode']});
        autocomplete.setFields(['address_component']);
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {

        var place = autocomplete.getPlace();


        adress = document.getElementById('calle').value;
        getLatitudeLongitude(showResult, adress)

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var val;
            if (addressType == 'street_number') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('numero').value = val;
            }
            if (addressType == 'route') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('calle').value = val;
            }
            if (addressType == 'locality') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('selectDistritos').value = val;
            }
            if (addressType == 'administrative_area_level_1') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('selectProvincia').value = val;
            }
            if (addressType == 'neighborhood') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('barrio').value = val;
            }
            if (addressType == 'sublocality_level_1') {
                val = place.address_components[i][componentForm[addressType]];
                document.getElementById('selectDistritos').value = val;
            }
            // if (addressType == 'administrative_area_level_2') {
            //     val = place.address_components[i][componentForm[addressType]];
            //     document.getElementById('selectDistritos').value = val;
            // }
            // if (addressType == 'country') {
            //     val = place.address_components[i][componentForm[addressType]];
            //     document.getElementById(addressType).value = val;
            // }
            // if (addressType == 'postal_code') {
            //     val = place.address_components[i][componentForm[addressType]];
            //     document.getElementById(addressType).value = val;
            // }
            // if (addressType == 'postal_code_suffix') {
            //     val = place.address_components[i][componentForm[addressType]];
            //     document.getElementById(addressType).value = val;
            // }
        }
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle(
                    {center: geolocation, radius: position.coords.accuracy});
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    function showResult(result) {
        lat = result.geometry.location.lat();
        lng = result.geometry.location.lng();
    }

    function getLatitudeLongitude(callback, address) {
        // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
        address = address || 'Ferrol, Galicia, Spain';
        // Initialize the Geocoder
        geocoder = new google.maps.Geocoder();
        if (geocoder) {
            geocoder.geocode({
                'address': address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    callback(results[0]);
                }
            });
        }
    }

    $(document).ready(function(){
        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById('lblimage').firstChild.data = fileName;
            alert('El archivo "' + fileName +  '" ha sido seleccionado.');
        });
    });

    window.onload = function() {

        $( "#busqueda" ).autocomplete({
            minLength: 3,
            autoFill: true,
            source: function (request, response) {
                //data :: JSON list defined

                var results = new Array();
                var needle = request.term.toLowerCase();

                var len = array_establecimientos.length;
                for(i = 0; i < len; ++i)
                {
                    var haystack = array_establecimientos[i].nombre.toLowerCase();
                    if(haystack.indexOf(needle) == 0 ||
                        haystack.indexOf(" " + needle) != -1)
                    {
                        results.push(array_establecimientos[i]);
                    }
                }
                response($.map(results, function (value, key)
                {

                    return {
                        label: value.nombre + " - " + value.tipo.toLocaleUpperCase(),
                        value: value.nombre,
                        key: value.key
                    }
                }));
            },
            select: function(dato, ui) {
                key_selected_institute = ui.item.key;
                document.getElementById('nombreinstitucion').value = ui.item.value;
            }});

        var provincias = firebase.database().ref("provincias").orderByKey();
        provincias.once("value")
            .then(function(snapshot) {
                snapshot.forEach(function(childSnapshot) {
                    var key = childSnapshot.key; // "ada"
                    var path = "provincias/" + key + "/partidos";

                    var partidos = firebase.database().ref(path).orderByKey();

                    partidos.once("value")
                        .then(function(snapshot) {
                            snapshot.forEach(function(childSnapshot) {
                                var partidos_key = childSnapshot.key; // "ada"
                                var partidos_path = path + "/" + partidos_key;

                                var merenderos_key = partidos_path + "/merendero"
                                var comederos_key = partidos_path + "/comederos"
                                var merenderos = firebase.database().ref(merenderos_key).orderByKey();
                                var comederos = firebase.database().ref(comederos_key).orderByKey();

                                merenderos.once("value")
                                    .then(function(snapshot) {
                                        snapshot.forEach(function(childSnapshot) {
                                            try{
                                            var merendero_key = childSnapshot.key; // "ada"

                                            const establecimientos = {
                                                nombre: 'establecimiento',
                                                key: 'path',
                                                tipo: 'merendero/comedero',
                                            };

                                            establecimientos.nombre = merendero_key;
                                            establecimientos.key = merenderos_key + "/" + merendero_key;
                                            establecimientos.tipo = "merendero - " + childSnapshot.val().DatosDeLaInstitucion.distrito; ;
                                            array_establecimientos.push(establecimientos);
                                            }
                                            catch(e){
                                                console.log(e);
                                            }
                                        });
                                    });

                                comederos.once("value")
                                    .then(function(snapshot) {
                                        snapshot.forEach(function(childSnapshot) {
                                            try{
                                            var comedero_key = childSnapshot.key; // "ada"

                                            const establecimientos = {
                                                nombre: 'establecimiento',
                                                key: 'path',
                                                tipo: 'merendero/comedero'
                                            };

                                            establecimientos.nombre = comedero_key;
                                            establecimientos.key = comederos_key + "/" + comedero_key;
                                            establecimientos.tipo = "comederos - " + childSnapshot.val().DatosDeLaInstitucion.distrito;;
                                            array_establecimientos.push(establecimientos);
                                            }catch(e){
                                                console.log(e);
                                            }
                                        });
                                    });
                            });
                        });

                });
            });

        geocoder = new google.maps.Geocoder();

        var nombreInstitucion = document.getElementById('nombreinstitucion').value;
        var imagen = document.getElementById('image').value;

        comboMunicipios = document.getElementById("selectDistritos");
        comboProvincias = document.getElementById("selectProvincia");
        // loadProv();

        document.getElementById("Subir").onclick = function Buscar()
        {
            const ref = firebase.storage().ref();
            const file = document.querySelector('#image').files[0]
            const name = (+new Date()) + '-' + file.name;
            const metadata = {
                contentType: file.type
            };
            const task = ref.child(name).put(file, metadata);

            waitingDialog.show("Cargando el archivo");

            task
                .then(snapshot => snapshot.ref.getDownloadURL())
                .then((url) => {
                    console.log(url);
                    //document.querySelector('#image').src = url;
                    imagen = url;
                    waitingDialog.hide();
                    //window.alert("Se ha subido correctamente la foto.")
                })
                .catch(console.error);
        };

        document.getElementById('submitFirebase').onclick = function (){
            if ( confirm( "Desea enviar el formulario?... " )) {
                funcionSbumitFirebase ();
            }
        }
        
        
        function funcionSbumitFirebase () {
            cnosole.log("Entro Al EnviarFormulario");
            $('form').validin();
            //Creación de variables
            var provincia = document.getElementById('selectProvincia').value;
            var distrito = document.getElementById('selectDistritos').value;
            var barrio = document.getElementById('barrio').value;
            var calle = document.getElementById('calle').value;
            var numero = document.getElementById('numero').value;
            var entrecalle = document.getElementById('entrecalle').value;
            if (nombreInstitucion == null || nombreInstitucion == "")
            {
                nombreInstitucion = document.getElementById('nombreinstitucion').value;
            }
            var descripcioninstitucion = document.getElementById('descripcioninstitucion').value;
            // var dia = document.getElementById('Dia').value;
            // var hora = document.getElementById('hora').value;
            var servicioqueofrece = document.getElementById('servicioqueofrece').value;
            var anio = document.getElementById('anio').value;
            var mes = document.getElementById('mes').value;
            var vinculo = document.getElementById('vinculo').value;
            var explicacion = document.getElementById('explicacion').value;
            var asistentes = document.getElementById('asistentes').value;
            var comensales = document.getElementById('comensales').value;
            var tipoenergia = document.getElementById('tipoenergia').value;
            var otrasactividades = document.getElementById('otrasactividades').value;
            var apoyo = document.getElementById('apoyo').checked;
            var terminalidadeducativa = document.getElementById('TerminalidadEducativa').checked;
            var actividadesculturales = document.getElementById('ActividadesCulturales').checked;
            var talleresoficios = document.getElementById('TalleresOficios').checked;
            var actividadesdeportivas = document.getElementById('ActividadesDeportivas').checked;
            var cuales = document.getElementById('cuales').value;
            var nombreresponsable = document.getElementById('nombreresponsable').value;
            var telefonoresponsable = document.getElementById('telefonoresponsable').value;
            var mailresponsable = document.getElementById('mailresponsable').value;

            var dialunes = document.getElementById('lunes').checked;
            var diamartes = document.getElementById('martes').checked;
            var diamiercoles = document.getElementById('miercoles').checked;
            var diajueves = document.getElementById('jueves').checked;
            var diaviernes = document.getElementById('viernes').checked;
            var diasabado = document.getElementById('sabado').checked;
            var diadomingo = document.getElementById('domingo').checked;
            var horalunes = document.getElementById('luneshora').value;
            var horamartes = document.getElementById('marteshora').value;
            var horamiercoles = document.getElementById('miercoleshora').value;
            var horajueves = document.getElementById('jueveshora').value;
            var horaviernes = document.getElementById('vierneshora').value;
            var horasabado = document.getElementById('sabadohora').value;
            var horadomingo = document.getElementById('domingohora').value;

            var servicioPath=[];

            if (provincia != "" && distrito != ""  )
            {
                if (servicioqueofrece == 'merendero' || servicioqueofrece == 'comederos')
                {
                    servicioPath[0] = servicioqueofrece;
                }
                else if (servicioqueofrece != "Servicio Que Ofrece")
                {
                    servicioPath[0] = 'merendero';
                    servicioPath[1] = 'comederos';
                }

                //Comienzo de insercion de datos en firebase
                var i;
                for (i = 0; i<servicioPath.length; i++)
                {
                    if (update == false)
                    {
                        firebase.database().ref('provincias/' + provincia + '/partidos/' + distrito + '/' + servicioPath[i] + '/' + nombreInstitucion + '/').set(
                            {
                                latitude: lat,
                                longitude: lng,
                                DatosDeLaInstitucion:
                                    {
                                        provincia: provincia,
                                        distrito: distrito,
                                        barrio: barrio,
                                        calle: calle,
                                        numero: numero,
                                        entrecalle: entrecalle,
                                        nombreinstitucion: nombreInstitucion,
                                        descripcioninstitucion: descripcioninstitucion
                                    },
                                DiasDeFuncionamiento:
                                    {
                                        lunes: {
                                            dia: dialunes,
                                            hora: horalunes
                                        },
                                        martes: {
                                            dia: diamartes,
                                            hora: horamartes
                                        },
                                        miercoles: {
                                            dia: diamiercoles,
                                            hora: horamiercoles
                                        },
                                        jueves: {
                                            dia: diajueves,
                                            hora: horajueves
                                        },
                                        viernes: {
                                            dia: diaviernes,
                                            hora: horaviernes
                                        },
                                        sabado: {
                                            dia: diasabado,
                                            hora: horasabado
                                        },
                                        domingo: {
                                            dia: diadomingo,
                                            hora: horadomingo
                                        }
                                    },
                                ServicioQueOfrece: servicioPath[i],
                                InicioDeActividad:
                                    {
                                        año: anio,
                                        mes: mes
                                    },
                                InstitucionVinculada:
                                    {
                                        vinculada: vinculo,
                                        descripcion: explicacion
                                    },
                                CantidadDeAsistentes: asistentes,
                                CantidadDeComensales: comensales,
                                TipoDeEnergia: tipoenergia,
                                OtrasActividades:
                                    {
                                        otras: otrasactividades,
                                        descripcion:
                                            {
                                                apoyo: apoyo,
                                                TerminalidadEducativa: terminalidadeducativa,
                                                ActividadesCulturales: actividadesculturales,
                                                TalleresOficios: talleresoficios,
                                                ActividadesDeportivas: actividadesdeportivas,
                                                otras: cuales
                                            }
                                    },
                                Responsable:
                                    {
                                        nombre: nombreresponsable,
                                        telefono: telefonoresponsable,
                                        email: mailresponsable
                                    },
                                Imagen: imagen
                            });
                    }
                    else
                    {
                        firebase.database().ref(key_selected_institute + "/").update(
                            {
                                latitude: lat,
                                longitude: lng,
                                DatosDeLaInstitucion:
                                    {
                                        provincia: provincia,
                                        distrito: distrito,
                                        barrio: barrio,
                                        calle: calle,
                                        numero: numero,
                                        entrecalle: entrecalle,
                                        nombreinstitucion: nombreInstitucion,
                                        descripcioninstitucion: descripcioninstitucion
                                    },
                                DiasDeFuncionamiento:
                                    {
                                        lunes: {
                                            dia: dialunes,
                                            hora: horalunes
                                        },
                                        martes: {
                                            dia: diamartes,
                                            hora: horamartes
                                        },
                                        miercoles: {
                                            dia: diamiercoles,
                                            hora: horamiercoles
                                        },
                                        jueves: {
                                            dia: diajueves,
                                            hora: horajueves
                                        },
                                        viernes: {
                                            dia: diaviernes,
                                            hora: horaviernes
                                        },
                                        sabado: {
                                            dia: diasabado,
                                            hora: horasabado
                                        },
                                        domingo: {
                                            dia: diadomingo,
                                            hora: horadomingo
                                        }
                                    },
                                ServicioQueOfrece: servicioPath[i],
                                InicioDeActividad:
                                    {
                                        año: anio,
                                        mes: mes
                                    },
                                InstitucionVinculada:
                                    {
                                        vinculada: vinculo,
                                        descripcion: explicacion
                                    },
                                CantidadDeAsistentes: asistentes,
                                CantidadDeComensales: comensales,
                                TipoDeEnergia: tipoenergia,
                                OtrasActividades:
                                    {
                                        otras: otrasactividades,
                                        descripcion:
                                            {
                                                apoyo: apoyo,
                                                TerminalidadEducativa: terminalidadeducativa,
                                                ActividadesCulturales: actividadesculturales,
                                                TalleresOficios: talleresoficios,
                                                ActividadesDeportivas: actividadesdeportivas,
                                                otras: cuales
                                            }
                                    },
                                Responsable:
                                    {
                                        nombre: nombreresponsable,
                                        telefono: telefonoresponsable,
                                        email: mailresponsable
                                    },
                                Imagen: imagen
                            });
                    }

                }
                //document.getElementById('setFirebaseData').submit();
                return true;
            }
            else
            {
                window.alert("Por favor revise que se encuentren rellenados Provincia y Distrito.")
            }
        };

        document.getElementById("buscar").onclick = function Buscar() {
            update = true;
            var row_selected = firebase.database().ref(key_selected_institute).orderByKey();
            row_selected.once("value")
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        switch (childSnapshot.key) {
                            case "DatosDeLaInstitucion":
                                document.getElementById('nombreinstitucion').disabled = true;
                                childSnapshot.forEach(function (subChildSnapshot) {
                                    switch (subChildSnapshot.key)
                                    {
                                        case "provincia":
                                            document.getElementById('selectProvincia').value = subChildSnapshot.val();
                                            break;
                                        case "distrito":
                                            document.getElementById('selectDistritos').value = subChildSnapshot.val();
                                            break;
                                        default:
                                            document.getElementById(subChildSnapshot.key).value = subChildSnapshot.val();
                                            break;
                                    }
                                });
                                break;
                            case "DiasDeFuncionamiento":
                                childSnapshot.forEach(function (subChildSnapshot) {
                                    switch (subChildSnapshot.key)
                                    {
                                        case "lunes":
                                            subChildSnapshot.forEach(function (diaChildSnapshot)
                                            {
                                                switch (diaChildSnapshot.key)
                                                {
                                                    case "dia":
                                                        document.getElementById('lunes').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "hora":
                                                        document.getElementById('luneshora').value = diaChildSnapshot.val();
                                                        break;
                                                }
                                            });
                                            break;
                                        case "martes":
                                            subChildSnapshot.forEach(function (diaChildSnapshot)
                                            {
                                                switch (diaChildSnapshot.key)
                                                {
                                                    case "dia":
                                                        document.getElementById('martes').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "hora":
                                                        document.getElementById('marteshora').value = diaChildSnapshot.val();
                                                        break;
                                                }
                                            });
                                            break;
                                        case "miercoles":
                                            subChildSnapshot.forEach(function (diaChildSnapshot)
                                            {
                                                switch (diaChildSnapshot.key)
                                                {
                                                    case "dia":
                                                        document.getElementById('miercoles').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "hora":
                                                        document.getElementById('miercoleshora').value = diaChildSnapshot.val();
                                                        break;
                                                }
                                            });
                                            break;
                                        case "jueves":
                                            subChildSnapshot.forEach(function (diaChildSnapshot)
                                            {
                                                switch (diaChildSnapshot.key)
                                                {
                                                    case "dia":
                                                        document.getElementById('jueves').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "hora":
                                                        document.getElementById('jueveshora').value = diaChildSnapshot.val();
                                                        break;
                                                }
                                            });
                                            break;
                                        case "viernes":
                                            subChildSnapshot.forEach(function (diaChildSnapshot)
                                            {
                                                switch (diaChildSnapshot.key)
                                                {
                                                    case "dia":
                                                        document.getElementById('viernes').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "hora":
                                                        document.getElementById('vierneshora').value = diaChildSnapshot.val();
                                                        break;
                                                }
                                            });
                                            break;
                                        case "sabado":
                                            subChildSnapshot.forEach(function (diaChildSnapshot)
                                            {
                                                switch (diaChildSnapshot.key)
                                                {
                                                    case "dia":
                                                        document.getElementById('sabado').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "hora":
                                                        document.getElementById('sabadohora').value = diaChildSnapshot.val();
                                                        break;
                                                }
                                            });
                                            break;
                                        case "domingo":
                                            subChildSnapshot.forEach(function (diaChildSnapshot)
                                            {
                                                switch (diaChildSnapshot.key)
                                                {
                                                    case "dia":
                                                        document.getElementById('domingo').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "hora":
                                                        document.getElementById('domingohora').value = diaChildSnapshot.val();
                                                        break;
                                                }
                                            });
                                            break;
                                    }
                                });
                                break;
                            case "ServicioQueOfrece":
                                document.getElementById('servicioqueofrece').value = childSnapshot.val();
                                document.getElementById('servicioqueofrece').disabled = true;
                                break;
                            case "InicioDeActividad":
                                childSnapshot.forEach(function (subChildSnapshot) {
                                    switch (subChildSnapshot.key)
                                    {
                                        case "año":
                                            document.getElementById('anio').value = subChildSnapshot.val();
                                            break;
                                        case "mes":
                                            document.getElementById('mes').value = subChildSnapshot.val();
                                            break;
                                    }
                                });
                                break;
                            case "InstitucionVinculada":
                                childSnapshot.forEach(function (subChildSnapshot) {
                                    switch (subChildSnapshot.key)
                                    {
                                        case "vinculada":
                                            document.getElementById('vinculo').value = subChildSnapshot.val();
                                            break;
                                        case "descripcion":
                                            document.getElementById('explicacion').value = subChildSnapshot.val();
                                            break;
                                    }
                                });
                                break;
                            case "CantidadDeAsistentes":
                                document.getElementById('asistentes').value = childSnapshot.val();
                                break;
                            case "CantidadDeComensales":
                                document.getElementById('comensales').value = childSnapshot.val();
                                break;
                            case "TipoDeEnergia":
                                document.getElementById('tipoenergia').value = childSnapshot.val();
                                break;
                            case "OtrasActividades":
                                childSnapshot.forEach(function (subChildSnapshot) {
                                    switch (subChildSnapshot.key)
                                    {
                                        case "otras":
                                            document.getElementById('otrasactividades').value = subChildSnapshot.val();
                                            break;
                                        case "descripcion":
                                            subChildSnapshot.forEach(function (diaChildSnapshot)
                                            {
                                                switch (diaChildSnapshot.key)
                                                {
                                                    case "apoyo":
                                                        document.getElementById('apoyo').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "TerminalidadEducativa":
                                                        document.getElementById('TerminalidadEducativa').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "ActividadesCulturales":
                                                        document.getElementById('ActividadesCulturales').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "TalleresOficios":
                                                        document.getElementById('TalleresOficios').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "ActividadesDeportivas":
                                                        document.getElementById('ActividadesDeportivas').checked = diaChildSnapshot.val();
                                                        break;
                                                    case "otras":
                                                        document.getElementById('cuales').value = diaChildSnapshot.val();
                                                        break;
                                                }
                                            });
                                            break;
                                    }
                                });
                                break;
                            case "Responsable":
                                childSnapshot.forEach(function (subChildSnapshot) {
                                    switch (subChildSnapshot.key)
                                    {
                                        case "nombre":
                                            document.getElementById('nombreresponsable').value = subChildSnapshot.val();
                                            break;
                                        case "telefono":
                                            document.getElementById('telefonoresponsable').value = subChildSnapshot.val();
                                            break;
                                        case "email":
                                            document.getElementById('mailresponsable').value = subChildSnapshot.val();
                                            break;
                                    }
                                });
                                break;
                            case "Imagen":
                                console.log(childSnapshot.val());
                                if (childSnapshot.val() != null) {
                                   
                                    var urlFoto = childSnapshot.val();
                                    var imagen = document.getElementById("imgCargada");

                                    
                                    imagen.setAttribute("src",urlFoto);
                                    imagen.setAttribute("style","visibility:visible;display:block;margin:10px auto;");
                                 
                                }
                                break;
                            case "latitude":
                                lat = childSnapshot.val();
                                break;
                            case "longitude":
                                lng = childSnapshot.val();
                                break;

                        }

                    });
                });
        }
        return false;
    };


</script>


    <div class="container">
        <div class="row ml-5 mr-5 mt-5 justify-content-arroun" >
            <h2>Formulario para la georeferenciación de merenderos y comedores</h2>
        </div>

        <form id="setFirebaseData" action="" method="POST">
            <div class="form-group row  justify-content-arround ml-5 mr-5  p-3 rounded" id="fondoa">
                <div class="col-12 ">
                    <h3>Busqueda</h3>
                </div>
                <div class="col-12 col-md-8">
                    <label for="busqueda"></label>
                    <input type="search" id="busqueda" name="busqueda" class="form-control"
                        placeholder="Buscar en las instituciones..."
                        onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32) || (event.charCode == 8) || (event.charCode >= 48 && event.charCode <= 57))'>
                </div>
                <div class="col-12 col-md-2 mt-4 mb-4">
                    <a class="btn btn-primary" id="buscar" onclick="Buscar()">Buscar</a>
                </div>
                <div class="col-12 ">
                    <h5>Datos de la institución</h5>
                </div>
                <div class="col-12 col-md-6">
                    <label for="nombreinstitucion"></label>
                    <input required type="text" name="nombreinstitucion" id="nombreinstitucion" class="form-control"
                        placeholder="Nombre de la institución"
                        onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32) || (event.charCode == 8) || (event.charCode >= 48 && event.charCode <= 57)  || event.charCode == 164 || event.charCode == 165))'>
                </div>
                <div class="col-12 col-md-6">
                    <label for="servicioqueofrece"></label>
                    <select id="servicioqueofrece"  class="form-control">
                        <option disabled selected>Servicio que ofrece</option>
                        <option value="merendero">Merendero</option>
                        <option value="comederos">Comedor</option>
                        <option value="merenderocomedor">Merendero y Comedor</option>
                    </select>
                </div>
                <div class="col-12 mt-3">
                    <textarea class="form-control" id="descripcioninstitucion" rows="3" placeholder="Descripción de la institución"></textarea>
                </div>
                <div class="col-12 mt-3">
                    <h6 for="vinculo">¿Está vinculado con alguna organización/institución?</h6>
                </div>
                <div class="col-3">
                    <select id="vinculo"  class="form-control">
                        <option value="si">Si</option>
                        <option value="no">No</option>
                    </select>
                </div>
                <div class="col-9">
                    <input type="text" name="explicacion" id="explicacion" class="form-control" placeholder="Nombre de la organización/institución">
                </div>
            </div>
            <div class="row  justify-content-arround ml-5 mr-5  p-3 rounded" id="fondob">
                <div class="col-12"><h5>Datos de localización</h5> </div>
                <div class="col-12 col-md-6">
                    <label for="calle"></label>
                    <input type="text" name="calle" id="calle" class="form-control" onFocus="geolocate()" placeholder="Calle">
                </div>
                <div class="col-6 col-md-2">
                    <label for="numero"></label>
                    <input type="text" name="numero" id="numero" class="form-control" placeholder="Número">
                </div>
                <div class="col-6 col-md-3">
                    <label for="barrio"></label>
                    <input type="text" name="barrio" id="barrio" class="form-control"placeholder="Barrio">
                </div>
                <div class="col-12 col-md-5">
                    <label for="selectProvincia"></label>
                    <input type="text" name="selectProvincia" id="selectProvincia" class="form-control" placeholder="Provincia" readonly>
                </div>
                <div class="col-12 col-md-5">
                    <label for="selectDistritos"></label>
                    <input type="text" name="selectDistritos" id="selectDistritos" class="form-control"placeholder="Distrito" readonly>
                </div>
                <div class="col-12 col-md-12">
                    <label for="entrecalle"></label>
                    <input type="text" name="entrecalle" id="entrecalle" class="form-control" placeholder="Entre calle y/o manzana">
                </div>
            </div>
            <div class="row mt-4 justify-content-arround ml-5 mr-5   p-3 rounded" id="fondoc">
                <div class="col-12"><h5>Otros datos importantes</h5></div>
                <div class="col-12"><h6>Días de funcionamiento y horario</h6></div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 ml-4 mt-3">
                    <input type="checkbox" class="form-check-input" name="lunes" id="lunes" value="lunes">Lunes</input> <br/>
                    <input type="checkbox" class="form-check-input" name="martes" id="martes" value="martes">Martes</input> <br/>
                    <input type="checkbox" class="form-check-input" name="miercoles" id="miercoles" value="miercoles">Miercoles</input> <br/>
                    <input type="checkbox" class="form-check-input" name="jueves" id="jueves" value="jueves">Jueves</input> <br/>
                    <input type="checkbox" class="form-check-input" name="viernes" id="viernes" value="viernes">Viernes</input> <br/>
                    <input type="checkbox" class="form-check-input" name="sabado" id="sabado" value="sabado">Sabado</input> <br/>
                    <input type="checkbox" class="form-check-input" name="domingo" id="domingo" value="domingo">Domingo</input> <br/>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 ml-4 mt-3">
                    <input type="text" name="hora" id="luneshora"    class="form-check-input" placeholder=" 00:00hs a 00:00hs"> <br/>
                    <input type="text" name="hora" id="marteshora"    class="form-check-input" placeholder=" 00:00hs a 00:00hs"> <br/>
                    <input type="text" name="hora" id="miercoleshora"  class="form-check-input"  placeholder=" 00:00hs a 00:00hs"> <br/>
                    <input type="text" name="hora" id="jueveshora"    class="form-check-input" placeholder=" 00:00hs a 00:00hs"> <br/>
                    <input type="text" name="hora" id="vierneshora"   class="form-check-input" placeholder=" 00:00hs a 00:00hs"> <br/>
                    <input type="text" name="hora" id="sabadohora"   class="form-check-input" placeholder=" 00:00hs a 00:00hs"> <br/>
                    <input type="text" name="hora" id="domingohora"   class="form-check-input" placeholder=" 00:00hs a 00:00hs"> <br/>
                </div>
                <div class="col-12 mt-3">
                    <label for="otrasactividades"><h6>Además de funcionar como merendero/comedor ¿Se desarrollan otras actividades en el lugar?</h6></label>
                </div>
                <div class="col-12 col-md-12">
                    <select id="otrasactividades"  class="form-control">
                        <option value="si">Sí</option>
                        <option value="no">No</option>
                    </select>
                </div>
                <div class="col-6 col-md-7 ml-4 mt-3">
                    <label for="respuestasi"><h6>Si respondió "Sí", por favor indique cuáles (puede elegir más de una)</h6> </label>
                    <br/>
                    <input type="checkbox" class="form-check-input" name="apoyo" id="apoyo" value="apoyo escolar">Apoyo Escolar</input>
                    <br/>
                    <input type="checkbox" class="form-check-input" name="TerminalidadEducativa" id="TerminalidadEducativa" value="Programa de terminalidad educativa">Programa de terminalidad educativa</input>
                    <br/>
                    <input type="checkbox" class="form-check-input" name="ActividadesCulturales" id="ActividadesCulturales" value="Actividades Culturales">Actividades Culturales</input>
                    <br/>
                    <input type="checkbox" class="form-check-input" name="TalleresOficios" id="TalleresOficios" value="Talleres de oficios">Talleres de oficios</input>
                    <br/>
                    <input type="checkbox" class="form-check-input" name="ActividadesDeportivas" id="ActividadesDeportivas" value="Actividades deportivas">Actividades deportivas</input>
                    <br/>
                    <input type="checkbox" class="form-check-input" name="otras" value="Otras">Otras</input>
                    <label for="otras"></label>
                    <input type="text" name="cuales" id="cuales" class="form-control" placeholder="Si desea especifique actividades (separadas por coma)" >
                </div>
                <div class="col-12 col-md-12  input-group mb-3 mt-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" >
                        <label class="custom-file-label" for="image" id="lblimage">Por favor adjunte una foto de la fachada de la institución</label>
                    </div>
                    <div class="input-group-append">
                        <input id="Subir" type="button" class="input-group-text" value="Subir" onclick="upload();"/>
                    </div>
                </div>
                <img id="imgCargada" class="img-thumbnail rounded" style="visibility: hidden;display:none;" src="" width="350px"/>
            </div>
            <div class="row row mt-4 justify-content-arround ml-5 mr-5   p-3 rounded" id="fondod">
                <div class="col-12"><h5>Datos internos de la red</h5></div>
                <div class="col-12 mt-3"><h6>Indique el año y mes en que comenzó a funcionar el merendero/comedor</h6></div>
                <div class="col-6">
                    <label for="anio"></label>
                    <select id="anio"  class="form-control">
                        <option disabled selected>Año</option>
                        <option value="sin información">Sin Informacion</option>
                        <option value="1990">1990</option>
                        <option value="1991">1991</option>
                        <option value="1992">1992</option>
                        <option value="1993">1993</option>
                        <option value="1994">1994</option>
                        <option value="1995">1995</option>
                        <option value="1996">1996</option>
                        <option value="1997">1997</option>
                        <option value="1998">1998</option>
                        <option value="1999">1999</option>
                        <option value="2000">2000</option>
                        <option value="2001">2001</option>
                        <option value="2002">2002</option>
                        <option value="2003">2003</option>
                        <option value="2004">2004</option>
                        <option value="2005">2005</option>
                        <option value="2006">2006</option>
                        <option value="2007">2007</option>
                        <option value="2008">2008</option>
                        <option value="2009">2009</option>
                        <option value="2010">2010</option>
                        <option value="2011">2011</option>
                        <option value="2012">2012</option>
                        <option value="2013">2013</option>
                        <option value="2014">2014</option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                        <option value="2029">2029</option>
                        <option value="2030">2030</option>
                    </select>
                </div>
                <div class="col-6">
                    <label for="mes"></label>
                    <select id = "mes" class="form-control" >
                        <option disabled selected>Mes</option>
                        <option value="sin información">Sin Informacion</option>
                        <option value="enero">Enero</option>
                        <option value="febrero">Febrero</option>
                        <option value="marzo">Marzo</option>
                        <option value="abril">Abril</option>
                        <option value="mayo">Mayo</option>
                        <option value="junio">Junio</option>
                        <option value="julio">Julio</option>
                        <option value="agosto">Agosto</option>
                        <option value="septiembre">Septiembre</option>
                        <option value="octubre">Octubre</option>
                        <option value="noviembre">Noviembre</option>
                        <option value="diciembre">Diciembre</option>
                    </select>
                </div>
                <div class="col-12 col-md-6 mt-3">
                    <h6 for="asistentes">¿Cuántas personas trabajan en el merendero/comedor?</h6>
                    <select id="asistentes"  class="form-control">
                        <option value="sin información">Sin Informacion</option>
                        <option value="1-10">1 y 10</option>
                        <option value="11-20">11 y 20</option>
                        <option value="20+">Mas de 20</option>
                    </select>
                </div>
                <div class="col-12 col-md-6 mt-3">
                    <h6 for="comensales">¿Cuántas personas asisten diariamente?</h6>
                    <select id="comensales"  class="form-control">
                        <option value="sin información">Sin Informacion</option>
                        <option value="1-50">1 y 50</option>
                        <option value="51-100">51 y 100</option>
                        <option value="101-150">101 y 150</option>
                        <option value="151-200">151 y 200</option>
                        <option value="200+">Mas de 200</option>
                    </select>
                </div>
                <div class="col-12 mt-3">
                    <h6 for="tipoenergia">¿Qué tipo de energía se utiliza?</h6>
                    <select id="tipoenergia"  class="form-control">
                        <option value="sin información">Sin Informacion</option>
                        <option value="natural">Gas Natural</option>
                        <option value="garrafa">Gas envasado (garrafa / tubo / tanque)</option>
                        <option value="lenia">Leña</option>
                        <option value="otro">Otro</option>
                    </select>
                </div>
            </div>
            <div class="row justify-content-arround ml-5 mr-5 mt-4 p-3 rounded" id="fondoe">
                <div class="col-12">
                    <label for="lbldatosresponsable"><h5>Datos de contacto del responsable del merendero/comedor</h5></label>
                </div>
                <div class="col-6">
                    <label for="nombreresponsable"></label>
                    <input type="text" name="nombreresponsable" id="nombreresponsable"  class="form-control" placeholder="Nombre">
                </div>
                <div class="col-6">
                    <label for="telefonoresponsable"></label>
                    <input type="text" name="telefonoresponsable" id="telefonoresponsable"  class="form-control" placeholder="Teléfono: código de área + número">
                </div>
                <div class="col-12">
                    <label for="mailresponsable"></label>
                    <input validate="email" type="email" name="temailresponsable" id="mailresponsable" class="form-control" placeholder="Correo Electrónico">
                </div>
            </div>
            <div class="row  mt-5 mb-5 justify-content-center">
                <div class="col-4 text-center  align-self-center">
                    <input id="submitFirebase" type="submit" class="button" text="submit">
    <!--                <a href="#" id="submitFirebase" class="button">submit</a>-->
                </div>
            </div>
        </form>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgVrOd5XOYmmS1tFz4UjKNX0hXZnjJ7P0&libraries=places&callback=initAutocomplete" async defer></script>
</body>
</html>
