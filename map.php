<!DOCTYPE html >
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <!-- <link rel="stylesheet" href="estilos.css"> -->
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=ABeeZee" rel="stylesheet">
    <link rel="stylesheet" href="css/map.css" >

    <title>Mapa de Comederos y Merenderos</title>
    
</head>

<body>
<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <div id="content-dialog"></div>
    </div>

</div>

<!-- Filtros -->
<div class="container">
    <form>
        <div id="filterCitys" class="row shadow p-3 mb-2 bg-light rounded mt-2 formulario">
            <div class="col-12"><h3>Parámetros de búsqueda</h3></div>    
            <div class="col-12 col-sm-3 p-2">
                <label style="color:#17a2b8"><small>Provincia</small></label>
                <select class="custom-select" id="provinciasList">
                </select>
            </div>
            <div class="col-12 col-sm-3  p-2 ">
                <label style="color:#17a2b8"><small>Localidad</small></label>
                <select class="custom-select" id="municipiosList">
                </select>
            </div>
            <div class="col-12 col-sm-3  p-2 ">
                <label style="color:#17a2b8"><small>Servicios</small></label>
                <select class="custom-select" id="serviciosList">
                    <option selected value=0>Merenderos</option>
                    <option selected value=1>Comedores</option>
                    <option selected value=2>Merendero y Comedor</option>
                    <option selected value=3>Todos</option>
                </select>
            </div>         
            <div class="col-12 col-sm-3  p-2 ">
                <label>&nbsp;</label><br>       
                <a id='buttonRefrescar' class="btn btn-info" style="color:#fff">Localizar</a>
            </div>       
        </div>
    </form>
</div>
<!-- /Filtros -->

<!-- Mapa -->
<div class="container-fluid" style="padding: 0 !important">
    <div class="row">
        <div id="map" class="col map-container img-fluid shadow p-3 mb-5"></div>
    </div>
</div>
<!-- /Mapa -->

<!-- Cards -->
<div class="container">
    <div id="cardsContainer" class="row" style="height: 500px;overflow-y:scroll;"/></div>
</div>
<!-- /Cards -->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase.js"></script>
<script src="js/initializeFirease.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt4-eHHq80AgKWr9fOgM9m7eZbyqZpYDI&callback=initMap">
</script>
<script src="js/map-js.js"></script>


</body>
</html>





















